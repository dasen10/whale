module whale

go 1.22.1

require xorm.io/xorm v1.3.8

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/go-sql-driver/mysql v1.8.1 // indirect
	github.com/google/uuid v1.5.0 // indirect
	github.com/gookit/filter v1.2.1 // indirect
	github.com/gookit/goutil v0.6.15 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.51.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/goccy/go-json v0.8.1 // indirect
	github.com/gofiber/fiber/v2 v2.52.2
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/golang/snappy v0.0.4 // indirect
	github.com/gookit/validate v1.5.2
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/lib/pq v1.10.9
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/wonderivan/logger v1.0.0
	github.com/xormplus/xorm v0.0.0-20210822100304-4e1d4fcc1e67
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0
	xorm.io/builder v0.3.13 // indirect
)
