package internal

import (
	"log"
	"whale/internal/app"
	"whale/internal/app/admin"
	"whale/internal/app/zhong"
	"whale/internal/load"
	"whale/internal/repository"
	"whale/pkg/bean"

	"github.com/joho/godotenv"
)

// 执行依赖注入
func SingletonModel() {

	// 载入配置文件
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}
	// 初始化注入容器
	beanContainer := bean.NewBeanContainer()
	// 注入数据库初始化
	beanContainer.SetSingleton("DBConnect", load.NewDBConnect().Ping().Generate(true))
	// 注入redis缓存初始化
	beanContainer.SetSingleton("RedisCache", load.NewCacheInstance())

	// 注入数据库增删改查公共库
	beanContainer.SetSingleton("Repository", &repository.Repository{})
	// 注入Admin业务模块
	beanContainer.SetSingleton("AdminRouter", admin.NewAdminRouter(beanContainer))
	// 注入Zhong业务模块
	beanContainer.SetSingleton("ZhongRouter", zhong.NewZhongRouter(beanContainer))

	// 注入web框架
	appRouter := &app.AppRouter{
		Host: "127.0.0.1",
		Port: ":8080",
	}
	beanContainer.SetSingleton("AppRouter", appRouter)

	// 注入所有依赖
	beanContainer.Entry(appRouter)

	// 启动项目
	appRouter.NewServer()
}
