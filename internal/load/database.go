package load

import (
	"os"

	_ "github.com/go-sql-driver/mysql"
	"go.uber.org/zap"
	"xorm.io/xorm"
	"xorm.io/xorm/log"
	"xorm.io/xorm/names"
)

var (
	engine *xorm.Engine
	err    error
)

type DBConnectType int

const (
	MYSQL   DBConnectType = iota // mysql数据库
	PGSQL                        // pgsql数据库链接
	SQLITE3                      // sqlite3 数据库链接
)

func (db DBConnectType) String() string {
	switch db {
	case MYSQL:
		return "mysql"
	case PGSQL:
		return "postgres"
	case SQLITE3:
		return "sqlite3"
	default:
		return "Unknown"
	}
}

// 数据库链接操作
type DBConnect struct{}

// 初始化链接数据库
func NewDBConnect() *DBConnect {
	// 数据库链接操作
	engine, err = xorm.NewEngine("mysql", os.Getenv("MYSQL_DSN"))
	if err != nil {
		panic("数据库链接失败" + err.Error())
	}
	return &DBConnect{}
}

// 返回数据库链接访问
func (db *DBConnect) Dao() *xorm.Engine {
	zap.L().Info("调用了数据库初始化对象")
	return engine
}

// 生成数据表
func (db *DBConnect) Generate(logger bool) *DBConnect {
	// 增加前缀信息
	tbMapper := names.NewPrefixMapper(names.SnakeMapper{}, "zs_")
	engine.SetTableMapper(tbMapper)
	if logger {
		engine.Logger().SetLevel(log.LOG_DEBUG)
	}
	AutoMigrate(engine)
	return db // 测试链接
}

func (db *DBConnect) Ping() *DBConnect {
	if err := engine.Ping(); err != nil {
		panic("数据库请求异常" + err.Error())
	}
	return db
}

type IDBConnect interface {
	// 返回数据库链接访问
	Dao() *xorm.Engine
	// 测试链接
	Ping() *DBConnect
	// 生成数据表
	Generate(logger bool) *DBConnect
}
