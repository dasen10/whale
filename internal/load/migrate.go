package load

import (
	admin "whale/internal/app/admin/model"
	zhongModel "whale/internal/app/zhong/model"

	"xorm.io/xorm"
)

func AutoMigrate(engine *xorm.Engine) {
	if err := engine.Sync2(
		new(admin.Account),
		new(admin.File),
		new(admin.Queue),
		new(admin.Oplog),
		new(admin.AdminAuth),
		new(admin.PageBlock),
		new(admin.Config),
		new(admin.Data),
		new(admin.DataKey),
		new(admin.Banner),

		new(zhongModel.Serving),
		new(zhongModel.ServingAlbum),
		new(zhongModel.Users),
		new(zhongModel.UserCars),
		new(zhongModel.Orders),
		new(zhongModel.OrderWrite),
		new(zhongModel.OrderReservation),
		new(zhongModel.Channels),
		new(zhongModel.ChannelInto),
		new(zhongModel.ChannelPopul),
		new(zhongModel.ChannelGoods),
		new(zhongModel.Salesman),
		new(zhongModel.SalesmanInto),
		new(zhongModel.Store),
		new(zhongModel.StoreStaff),
		new(zhongModel.StoreWrite),
		new(zhongModel.StoreAlbum),
		new(zhongModel.WithdrawalRecord),
	); err != nil {
		panic("数据表同步失败" + err.Error())
	}
}
