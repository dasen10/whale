package load

import "go.uber.org/zap"

// 缓存
type RedisCache struct{}

func NewCacheInstance() *RedisCache {
	zap.L().Info("初始化redis缓存")
	return &RedisCache{}
}

// 获取redis对象
func (redis *RedisCache) Cache() {
	panic("not implemented") // TODO: Implement
}

type IRedisCache interface {
	// 获取redis对象
	Cache()
}
