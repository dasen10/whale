package models

import (
	"time"
)

// 转换存储时间单位
type XormTime time.Time

func (x XormTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + time.Time(x).Format("2006-01-02 15:04:05") + `"`), nil
}

type BaseModel struct {
	ID       int       `xorm:"pk autoincr 'id' comment('数据id')" json:"id"`
	CreateAt XormTime  `xorm:"created comment('创建日期')" json:"create_at"`
	UpdateAt XormTime  `xorm:"updated comment('数据更新发生日期')" json:"update_at"`
	Deleted  time.Time `xorm:"deleted comment('数据日期状态')" json:"deleted"`
}
