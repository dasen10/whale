package middleware

import (
	"whale/pkg/jwt"
	"whale/pkg/response"

	"github.com/gofiber/fiber/v2"
)

func AdminTokenMiddeware(ctx *fiber.Ctx) error {
	// 读取token信息
	token := ctx.Get("Authorization")
	info, err := jwt.ParseJwtToken(token)
	if err != nil {
		return response.Error(ctx, 10009, err.Error())
	}
	ctx.Locals("admin_id", info.AdminId)
	ctx.Locals("admin_name", info.AdminName)
	return ctx.Next()
}
