package middleware

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"github.com/gookit/validate"
	"go.uber.org/zap"
)

type ValidateCtx struct {
	*fiber.Ctx
}

// 验证函数
func (c *ValidateCtx) Validate(data interface{}) error {
	if err := c.BodyParser(&data); err != nil {
		return errors.New(err.Error())
	}
	zap.L().Debug("中间件执行了")
	v := validate.Struct(data)
	if !v.Validate() {
		zap.L().Info("参数验证成功")
		return errors.New(v.Errors.One())
	}
	return nil
}

func ValidateMiddleware(ctx *fiber.Ctx) error {
	validateCtx := ValidateCtx{ctx}
	zap.L().Debug("中间件执行了")
	// 中间件函数
	return validateCtx.Next()
}
