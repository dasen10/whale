package app

import (
	"os"
	"whale/internal/app/admin"
	"whale/internal/app/zhong"
	"whale/internal/middleware"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

type AppRouter struct {
	AdminRouter admin.IAdminRouter `bean:"AdminRouter"`
	ZhongRouter zhong.IZhongRouter `bean:"ZhongRouter"`
	Host        string
	Port        string
}

func (app *AppRouter) NewServer() {
	fiberApp := fiber.New()
	// 增加日志检测
	fiberApp.Use(logger.New(logger.Config{
		Format:     "[${ip}]:${port} ${status} - ${method} ${path}\n",
		TimeFormat: "2006-01-02 15:04:05",
	}))

	fiberApp.Use(middleware.ValidateMiddleware)
	fiberApp.Use(cors.New(cors.Config{
		AllowOrigins: "*",                                // 允许所有域名访问
		AllowMethods: "GET,POST,HEAD,PUT,DELETE,OPTIONS", // 允许的方法
	}))
	fiberApp.Static("/", os.Getenv("UPLOAD_FILE_PATH"))
	fiberApp.Get("/ping", func(c *fiber.Ctx) error {
		return c.JSON("333322221111")
	})
	// 载入模块路由
	app.AdminRouter.AdminRouterInit(fiberApp)
	// 载入模块路由
	app.ZhongRouter.ZhongRouterInit(fiberApp)
	// .ZhongRouterInit(fiberApp)
	// 启动项目
	fiberApp.Listen(app.Port)
}
