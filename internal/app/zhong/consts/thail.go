package consts

import (
	"whale/internal/models"
	w "whale/internal/whail"
)

type ThailListReq struct {
	w.PagingReq
	w.SearchKeywordReq
}

type ThailInfoReq struct {
	w.ByIdReq
}

type ThailCreateReq struct {
	Name         string  `json:"name,omitempty" validate:"required"`
	Cover        string  `json:"cover,omitempty" validate:"required"`
	Price        float64 `json:"price,omitempty" validate:"required"`
	StoreIds     string  `json:"store_ids,omitempty" validate:"required"`
	PreBookText  string  `json:"pre_book_text,omitempty" validate:"required"`
	BuyText      string  `json:"buy_text,omitempty" validate:"required"`
	Content      string  `json:"content,omitempty" validate:"required"`
	Subtitle     string  `json:"subtitle,omitempty" validate:"-"`
	VirtuallyNum int     `json:"virtually_num,omitempty" validate:"-"`
}

type ThailUpdateReq struct {
	w.ByIdReq
	ThailCreateReq
}

type ThailListRes struct {
	Id           int             `json:"id,omitempty"`
	Name         string          `json:"name,omitempty"`
	CreateAt     models.XormTime `json:"create_at,omitempty"`
	Cover        string          `json:"cover,omitempty"`
	Price        float64         `json:"price,omitempty"`
	StoreIds     string          `json:"store_ids,omitempty"`
	PreBookText  string          `json:"pre_book_text,omitempty"`
	BuyText      string          `json:"buy_text,omitempty"`
	Content      string          `json:"content,omitempty"`
	Subtitle     string          `json:"subtitle,omitempty"`
	VirtuallyNum int             `json:"virtually_num"`
	Status       int             `json:"status"`
	Sales        int             `json:"sales"`
}

type ThailInfoRes struct {
	Id           int             `json:"id,omitempty"`
	Name         string          `json:"name,omitempty"`
	CreateAt     models.XormTime `json:"create_at,omitempty"`
	Cover        string          `json:"cover,omitempty"`
	Price        float64         `json:"price,omitempty"`
	StoreIds     string          `json:"store_ids,omitempty"`
	PreBookText  string          `json:"pre_book_text,omitempty"`
	BuyText      string          `json:"buy_text,omitempty"`
	Content      string          `json:"content,omitempty"`
	Subtitle     string          `json:"subtitle,omitempty"`
	VirtuallyNum int             `json:"virtually_num"`
	Status       int             `json:"status,omitempty"`
	Sales        int             `json:"sales"`
}
