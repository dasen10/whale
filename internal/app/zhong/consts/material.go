package consts

import w "whale/internal/whail"

type MaterialListReq struct {
	w.PagingReq
}

type MaterialCreateReq struct {
}

type MaterialUpdateReq struct {
}

type MaterialListRes struct {
}

type MaterialInfoRes struct {
}
