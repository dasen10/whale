package zhong

import (
	"whale/internal/app/zhong/admin/business"
	"whale/internal/middleware"
	"whale/pkg/bean"

	"github.com/gofiber/fiber/v2"
)

type IZhongRouter interface {
	ZhongRouterInit(app *fiber.App)
}

type ZhongRouter struct {
	ThailHandler business.IThail `bean:"ThailHandler"`
}

func (z *ZhongRouter) ZhongRouterInit(app *fiber.App) {
	adminApi := app.Group("/admin/v1/api")
	{
		adminApi.Use(middleware.AdminTokenMiddeware)

		adminApi.Get("/business/thail.list", z.ThailHandler.List)
		adminApi.Get("/business/thail.info", z.ThailHandler.Info)
		adminApi.Post("/business/thail.create", z.ThailHandler.Create)
		adminApi.Post("/business/thail.update", z.ThailHandler.Update)
		adminApi.Post("/business/thail.set", z.ThailHandler.Set)
		adminApi.Post("/business/thail.delete", z.ThailHandler.Deleted)
	}
}

func NewZhongRouter(beanContainer *bean.BeanContainer) *ZhongRouter {
	beanContainer.SetSingleton("ThailHandler", &business.ThailHandler{})
	return &ZhongRouter{}
}
