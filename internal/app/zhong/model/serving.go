package model

import (
	"whale/internal/models"
)

// 服务表
type Serving struct {
	models.BaseModel `xorm:"extends"`
	Name             string  `xorm:"varchar(120) notnull comment('服务名称')" json:"name,omitempty" `
	Cover            string  `xorm:"varchar(500) notnull comment('封面照片')" json:"cover,omitempty"`
	StoreIds         string  `xorm:"varchar(120) notnull comment('关联门店')" json:"store_ids,omitempty"`
	Price            float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('基础价格')" json:"price,omitempty"`
	PreBookText      string  `xorm:"text notnull comment('预约须知')" json:"pre_book_text,omitempty"`
	BuyText          string  `xorm:"text notnull comment('购买须知')" json:"buy_text,omitempty"`
	Content          string  `xorm:"text notnull comment('服务详情')" json:"content,omitempty"`
	Subtitle         string  `xorm:"varchar(300) notnull comment('服务副标题')" json:"subtitle,omitempty"`
	Sales            int     `xorm:"BIGINT(20) notnull  comment('销量')" json:"sales,omitempty"`
	VirtuallyNum     int     `xorm:"BIGINT(20) notnull  comment('虚拟销量')" json:"virtually_num,omitempty"`
	Status           int     `xorm:"TINYINT(3) default(1) comment('2=下架 1=上架')"`
}

// 服务相册表
type ServingAlbum struct {
	Id        int    `xorm:"pk autoincr comment('数据id')" json:"id"`
	ServingId int    `xorm:"BIGINT(20) notnull comment('关联服务id')" json:"serving_id"`
	ImageSrc  string `xorm:"VARCHAR(300) comment('图片地址')" json:"image_src"`
	Weight    int    `xorm:"BIGINT(10) notnull default(0) comment('排序权重')" json:"weight"`
}

// 用户表
type Users struct {
	models.BaseModel `xorm:"extends"`
	WeappOpenid      string `xorm:"varchar(120) notnull comment('小程序openid')" json:"weapp_openid,omitempty"`
	Nickname         string `xorm:"varchar(120) notnull comment('用户昵称')" json:"nickname,omitempty"`
	Headimg          Store  `xorm:"varchar(120) notnull comment('用户头像')" json:"headimg,omitempty"`
	Mobile           string `xorm:"varchar(120) notnull comment('手机号码')" json:"mobile,omitempty"`
	ChannelId        int    `xorm:"BIGINT(20) defautl(0) comment('渠道ID 0=不是渠道')" json:"channel_id,omitempty"`
	ChannelName      string `xorm:"varchar(120) comment('渠道商名称')" json:"channel_name,omitempty"`
	OrderAmountTotal int    `xorm:"BIGINT(20) default(0) comment('订单交易统计')" json:"order_amount_total,omitempty"`
	OrderReturnTotal int    `xorm:"BIGINT(20) default(0) comment('订单退款统计')" json:"order_return_total,omitempty"`
	NotUsedOrder     int    `xorm:"BIGINT(20) default(0) comment('未使用订单统计')" json:"not_used_order,omitempty"`
	Status           int    `xorm:"TINYINT(3) default(1) comment('2=拉黑 1=正常')" json:"status,omitempty"`
}

// 用户车辆信息表
type UserCars struct {
	models.BaseModel `xorm:"extends"`
	UserId           int    `xorm:"BINGINT(20) comment('关联用户id')" json:"user_id,omitempty"`
	PlateNumber      string `xorm:"varchar(120) comment('车牌号')" json:"plate_number,omitempty"`
	IsEnergy         int    `xorm:"TINYINT(3) default(0) comment('1=新能源 0=燃油车')" json:"is_energy,omitempty"`
}

// 订单表
type Orders struct {
	models.BaseModel `xorm:"extends"`
	OrderCode        string `xorm:"varchar(120) unique comment('订单编号')" json:"order_code,omitempty"`

	UserId   int    `xorm:"BIGINT(20)  comment('用户编号')" json:"user_id,omitempty"`
	UserName string `xorm:"varchar(200) notnull comment('用户昵称')" json:"user_name,omitempty"`
	Headimg  string `xorm:"varchar(300) notnull comment('购买人头像')" json:"headimg,omitempty"`

	ServingId   int    `xorm:"BIGINT(20)  comment('服务编号')" json:"serving_id,omitempty"`
	ServingName string `xorm:"varchar(200) notnull comment('服务名称')" json:"serving_name,omitempty"`
	Cover       string `xorm:"varchar(300) notnull comment('服务封面')" json:"cover,omitempty"`

	StoreIds string `xorm:"varchar(300) comment('可选门店')" json:"store_ids,omitempty"`

	OrderName     string  `xorm:"varchar(300) comment('订单名称')" json:"order_name,omitempty"`
	OrderSubtitle string  `xorm:"varchar(300) comment('订单副标题')" json:"order_subtitle,omitempty"`
	BasisPirce    float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('基础价格')" json:"basis_pirce,omitempty"`
	OrderPirce    float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('订单价格')" json:"order_pirce,omitempty"`
	PayPirce      float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('实付金额')" json:"pay_pirce,omitempty"`

	ChannelId     int     `xorm:"BIGINT(20)  comment('渠道编号')" json:"channel_id,omitempty"`
	ChannelName   string  `xorm:"varchar(300) comment('渠道名称')" json:"channel_name,omitempty"`
	ChannelProfit float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('渠道利润')" json:"channel_profit,omitempty"`

	PayType   int             `xorm:"TINYINT(3) default(1)  comment('支付类型 1=微信支付')" json:"pay_type,omitempty"`
	PayCode   string          `xorm:"varchar(300) comment('支付编号')" json:"pay_code,omitempty"`
	PayTime   models.XormTime `xorm:" comment('支付时间')" json:"pay_time,omitempty"`
	PayStatus int             `xorm:"TINYINT(3) default(0) comment('支付状态 0=未支付  1=支付中 2=支付成功 3=支付失败')" json:"pay_status,omitempty"`

	Status int `xorm:"TINYINT(3) default(1) comment('订单状态 1=等待支付 2=等待核销 3=已完成 4=已取消')" json:"status,omitempty"`
}

// 订单核销记录
type OrderWrite struct {
	models.BaseModel
	OrderCode string `xorm:"varchar(120) unique comment('订单编号')" json:"order_code,omitempty"`

	WriteTime           models.XormTime `xorm:" comment('核销时间')" json:"write_time,omitempty"`
	WriteStoreId        int             `xorm:"BIGINT(20) comment('核销门店id')" json:"write_store_id,omitempty"`
	WriteStoreName      string          `xorm:"varchar(300) comment('核销门店名称')" json:"write_store_name,omitempty"`
	WriteStoreStaffId   int             `xorm:"BIGINT(20) comment('核销员工id')" json:"write_store_staff_id,omitempty"`
	WriteStoreStaffName string          `xorm:"varchar(300) comment('核销员工名称')" json:"write_store_staff_name,omitempty"`
	WriteStatus         int             `xorm:"TINYINT(3) default(0) comment('核销状态 0=等待核销  1=核销完成 2=核销失败')" json:"write_status,omitempty"`
}

// 订单预约记录
type OrderReservation struct {
	models.BaseModel `xorm:"extends"`
	OrderCode        string `xorm:"varchar(120) unique comment('订单编号')" json:"order_code,omitempty"`

	Contacts string `xorm:"varchar(300) comment('联系人')" json:"contacts,omitempty"`
	Mobile   string `xorm:"varchar(300) comment('联系方式')" json:"mobile,omitempty"`

	ReserDate         string `xorm:"varchar(300) comment('预约时间')" json:"reser_date,omitempty"`
	ReserStoreId      int    `xorm:"BIGINT(20)  comment('预约门店')" json:"reser_store_id,omitempty"`
	ReserStoreName    string `xorm:"varchar(300) comment('门店名称')" json:"reser_store_name,omitempty"`
	ReserStoreAddress string `xorm:"varchar(300) comment('门店地址')" json:"reser_store_address,omitempty"`
	ReserCardId       int    `xorm:"BIGINT(20)  comment('车牌编号')" json:"reser_card_id,omitempty"`
	ReserCardNumber   string `xorm:"varchar(300) comment('车牌号码')" json:"reser_card_number,omitempty"`
}

type Point struct {
	Lat float64 `xorm:"lat"`
	Lng float64 `xorm:"lng"`
}

// 渠道表
type Channels struct {
	models.BaseModel `xorm:"extends"`
	Userid           int    `xorm:"BIGINT(20) comment('用户id')" json:"userid,omitempty"`
	Type             int    `xorm:"TINYINT(3) default(1) comment('认证主体 1=个人 2=公司 3=个体工商户')" json:"type,omitempty"`
	SubjectName      string `xorm:"varchar(300) comment('主体名称')" json:"subject_name,omitempty"`
	SubjectNumber    string `xorm:"varchar(300) comment('证件号码')" json:"subject_number,omitempty"`
	Account          string `xorm:"varchar(300) comment('登录账号')" json:"account,omitempty"`
	Password         string `xorm:"varchar(300) comment('登录密码')" json:"password,omitempty"`
	Salt             string `xorm:"varchar(300) comment('加密密钥')" json:"salt,omitempty"`
	Address          string `xorm:"varchar(300) comment('所在地址')" json:"address,omitempty"`
	Location         Point  `xorm:"notnull comment('经纬度')" json:"location,omitempty"`

	SalesmanId int    `xorm:"BIGINT(20) comment('业务员id')" json:"salesman_id,omitempty"`
	SalesName  string `xorm:"varchar(300) comment('业务员名称')" json:"sales_name,omitempty"`

	AlipayAccount string `xorm:"varchar(300)  comment('支付宝收款账号')"`
	AlipayQrcode  string `xorm:"varchar(300)  comment('支付宝收款二维码')"`

	WepayAccount string `xorm:"varchar(300)  comment('微信收款账号')"`
	WepayQrcode  string `xorm:"varchar(300)  comment('微信收款二维码')"`

	BankName   string `xorm:"varchar(300)  comment('银行名称')"`
	Name       string `xorm:"varchar(300) comment('持卡人姓名')"`
	BankNumber string `xorm:"varchar(300) comment('银行卡号')"`

	Annex         string  `xorm:"text comment('附件资料，提高审核通过率')" json:"annex,omitempty"`
	Authen        int     `xorm:"TINYINT(3) default(0) comment('认证状态 0=未提交 1=审核中 2=审核通过 3=审核失败')" json:"authen,omitempty"`
	Balance       float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('钱包')" json:"balance,omitempty"`
	FreezeBalance float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('冻结资金')" json:"freeze_balance,omitempty"`
	Remark        string  `xorm:"varchar(300) comment('备注信息')" json:"remark,omitempty"`
	Status        int     `xorm:"TINYINT(3) default(1) comment('渠道状态 1=正常 2=禁用')" json:"status,omitempty"`
}

// 渠道推广明细
type ChannelPopul struct {
	models.BaseModel `xorm:"extends"`
	UserId           int    `xorm:"BIGINT(20) comment('访问人id')" json:"user_id,omitempty"`
	UserName         string `xorm:"varchar(300) comment('访问人昵称')" json:"user_name,omitempty"`
	Headimg          string `xorm:"varchar(300) comment('访问人头像')" json:"headimg,omitempty"`
	Uri              string `xorm:"varchar(300) comment('访问连接')" json:"uri,omitempty"`
	Page             string `xorm:"varchar(300) comment('访问页面')" json:"page,omitempty"`
	ChannelId        int    `xorm:"BIGINT(20) comment('渠道id')" json:"channel_id,omitempty"`
	ChannelName      string `xorm:"varchar(300) comment('渠道名称')" json:"channel_name,omitempty"`
}

// 渠道商品表
type ChannelGoods struct {
	models.BaseModel `xorm:"extends"`
	ServingId        int     `xorm:"BIGINT(20) comment('服务id')" json:"serving_id,omitempty"`
	ServingName      string  `xorm:"varchar(300) comment('服务名称')" json:"serving_name,omitempty"`
	ChannelId        int     `xorm:"BIGINT(20) comment('渠道id')" json:"channel_id,omitempty"`
	ChannelName      string  `xorm:"varchar(300)  comment('渠道名称')" json:"chanenl_name,omitempty"`
	BasisPirce       float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('基础价格')" json:"basis_pirce,omitempty"`
	MarkPirce        float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('商品价格')" json:"mark_pirce,omitempty"`

	Sales  int `xorm:"BIGINT(20) notnull  comment('销量')" json:"sales,omitempty"`
	Status int `xorm:"TINYINT(3) default(1) comment('2=下架 1=上架')" json:"status,omitempty"`
}

// 渠道分成记录表
type ChannelInto struct {
	models.BaseModel `xorm:"extends"`
	ChannelId        int    `xorm:"BIGINT(20) comment('渠道id')" json:"channel_id,omitempty"`
	ChannelName      string `xorm:"varchar(300)  comment('渠道名称')" json:"chanenl_name,omitempty"`

	ChannelProfit float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('渠道利润')" json:"channel_profit,omitempty"`
	Status        int     `xorm:"TINYINT(3) default(1) comment('2=可提现 1=冻结中')" json:"status,omitempty"`
}

// 业务员表
type Salesman struct {
	models.BaseModel `xorm:"extends"`
	UserId           int    `xorm:"BIGINT(20) comment('业务员id')" json:"user_id,omitempty"`
	UserName         string `xorm:"varchar(300) comment('业务员昵称')" json:"user_name,omitempty"`
	Headimg          string `xorm:"varchar(300) comment('业务员头像')" json:"headimg,omitempty"`
	Account          string `xorm:"varchar(300) comment('登录账号')" json:"account,omitempty"`
	Password         string `xorm:"varchar(300) comment('登录密码')" json:"password,omitempty"`
	Salt             string `xorm:"varchar(300) comment('加密密钥')" json:"salt,omitempty"`

	AlipayAccount string `xorm:"varchar(300)  comment('支付宝收款账号')"`
	AlipayQrcode  string `xorm:"varchar(300)  comment('支付宝收款二维码')"`

	WepayAccount string `xorm:"varchar(300)  comment('微信收款账号')"`
	WepayQrcode  string `xorm:"varchar(300)  comment('微信收款二维码')"`

	BankName   string `xorm:"varchar(300)  comment('银行名称')"`
	Name       string `xorm:"varchar(300) comment('持卡人姓名')"`
	BankNumber string `xorm:"varchar(300) comment('银行卡号')"`

	Authen        int     `xorm:"TINYINT(3) default(0) comment('认证状态 0=未提交 1=审核中 2=审核通过 3=审核失败')" json:"authen,omitempty"`
	Balance       float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('钱包')" json:"balance,omitempty"`
	FreezeBalance float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('冻结资金')" json:"freeze_balance,omitempty"`
	Remark        string  `xorm:"varchar(300) comment('备注信息')" json:"remark,omitempty"`
	Status        int     `xorm:"TINYINT(3) default(1) comment('业务员状态 1=正常 2=禁用')" json:"status,omitempty"`
}

// 业务员分成记录
type SalesmanInto struct {
	models.BaseModel `xorm:"extends"`
	SalesmanId       int     `xorm:"BIGINT(20) comment('业务员id')" json:"salesman_id,omitempty"`
	SalesName        string  `xorm:"varchar(300) comment('业务员名称')" json:"sales_name,omitempty"`
	IntoType         int     `xorm:"TINYINT(3) default(1) comment('分成类型 1=签约奖励 2=激活奖励')"`
	Content          string  `xorm:"text  comment('分成描述')"`
	IntoPrice        float64 `xorm:"DECIMAL(10,2) notnull default(0.00) comment('分成金额')"`
	ChannelId        int     `xorm:"BIGINT(20) comment('渠道id')" json:"channel_id,omitempty"`
	ChannelName      string  `xorm:"varchar(300)  comment('渠道名称')" json:"chanenl_name,omitempty"`
	Status           int     `xorm:"TINYINT(3) default(1) comment('分成记录 1=正常 2=冻结')" json:"status,omitempty"`
}

// 门店表
type Store struct {
	models.BaseModel `xorm:"extends"`
	UserId           int    `xorm:"BIGINT(20) comment('管理员id')" json:"user_id,omitempty"`
	UserName         string `xorm:"varchar(300) comment('管理员昵称')" json:"user_name,omitempty"`
	Headimg          string `xorm:"varchar(300) comment('管理员头像')" json:"headimg,omitempty"`
	Account          string `xorm:"varchar(300) comment('登录账号')" json:"account,omitempty"`
	Password         string `xorm:"varchar(300) comment('登录密码')" json:"password,omitempty"`
	Salt             string `xorm:"varchar(300) comment('加密密钥')" json:"salt,omitempty"`
	Address          string `xorm:"varchar(300) comment('门店地址')" json:"address,omitempty"`
	Location         []byte `xorm:"POINT notnull comment('经纬度')" json:"location,omitempty"`
	Cover            string `xorm:"varchar(300) comment('门店门头照片')"`
	BookableTime     string `xorm:"varchar(300) comment('可预约时间')"`
	Content          string `xorm:"text comment('门店介绍')"`
	Remark           string `xorm:"varchar(300) comment('备注信息')" json:"remark,omitempty"`
	Status           int    `xorm:"TINYINT(3) default(1) comment('门店状态 1=正常 2=禁用')" json:"status,omitempty"`
}

// 门店员工表
type StoreStaff struct {
	models.BaseModel `xorm:"extends"`
	StoreId          int    `xorm:"BIGINT(20) comment('门店id')"`
	StoreName        string `xorm:"varchar(300) comment('门店名称')"`
	UserId           int    `xorm:"BIGINT(20) comment('管理员id')" json:"user_id,omitempty"`
	UserName         string `xorm:"varchar(300) comment('管理员昵称')" json:"user_name,omitempty"`
	Headimg          string `xorm:"varchar(300) comment('管理员头像')" json:"headimg,omitempty"`
	Account          string `xorm:"varchar(300) comment('登录账号')" json:"account,omitempty"`
	Password         string `xorm:"varchar(300) comment('登录密码')" json:"password,omitempty"`
	Salt             string `xorm:"varchar(300) comment('加密密钥')" json:"salt,omitempty"`

	IsAuth   int `xorm:"comment('是否管理权限 0=无管理权限 1=有管理权限')"`
	IsDelete int `xorm:"comment('是否可删除 0=可删除 1=不可删除')"`
	Status   int `xorm:"TINYINT(3) default(1) comment('员工状态 1=正常 2=禁用')" json:"status,omitempty"`
}

// 门店核销记录表
type StoreWrite struct {
	models.BaseModel `xorm:"extends"`
	OrderCode        string `xorm:"varchar(120) unique comment('订单编号')" json:"order_code,omitempty"`

	StaffId      int    `xorm:"BIGINT(20) comment('员工id')"`
	StaffName    string `xorm:"varchar(300) comment('员工名称')"`
	StaffHeadimg string `xorm:"varchar(300) notnull comment('员工头像')"`

	StoreId   int    `xorm:"BIGINT(20) comment('门店id')"`
	StoreName string `xorm:"varchar(300) comment('门店名称')"`

	UserId   int    `xorm:"BIGINT(20)  comment('用户编号')" json:"user_id,omitempty"`
	UserName string `xorm:"varchar(200) notnull comment('用户昵称')" json:"user_name,omitempty"`
	Headimg  string `xorm:"varchar(300) notnull comment('购买人头像')" json:"headimg,omitempty"`

	ServingId   int    `xorm:"BIGINT(20)  comment('服务编号')" json:"serving_id,omitempty"`
	ServingName string `xorm:"varchar(200) notnull comment('服务名称')" json:"serving_name,omitempty"`
	Cover       string `xorm:"varchar(300) notnull comment('服务封面')" json:"cover,omitempty"`

	Remark string `xorm:"varchar(300) comment('核销备注')" json:"remark,omitempty"`
	Status int    `xorm:"TINYINT(3) default(1) comment('核销状态 1=核销 2=撤回核销')" json:"status,omitempty"`
}

// 门店相册表
type StoreAlbum struct {
	Id       int    `xorm:"pk autoincr comment('数据id')" json:"id"`
	StoreId  int    `xorm:"BIGINT(20) notnull comment('门店id')"`
	ImageSrc string `xorm:"VARCHAR(300) comment('图片地址')" json:"image_src"`
	Weight   int    `xorm:"BIGINT(10) notnull default(0) comment('排序权重')" json:"weight"`
}

// 提现记录表
type WithdrawalRecord struct {
	models.BaseModel `xorm:"extends"`
	UserId           int    `xorm:"BIGINT(20)  comment('用户编号')" json:"user_id,omitempty"`
	UserName         string `xorm:"varchar(200) notnull comment('用户昵称')" json:"user_name,omitempty"`
	Headimg          string `xorm:"varchar(300) notnull comment('购买人头像')" json:"headimg,omitempty"`
	WeappOpenid      string `xorm:"varchar(120) notnull comment('小程序openid')" json:"weapp_openid,omitempty"`

	// 身份
	Identity int     `xorm:" comment('申请人身份 1=业务员 2=渠道商')"`
	Code     string  `xorm:" comment('提现申请单号')"`
	Money    float64 `xorm:" comment('提现金额')"`

	ChannelId     int    `xorm:"BIGINT(20) comment('渠道id')" json:"channel_id,omitempty"`
	ChannelName   string `xorm:"varchar(300)  comment('渠道名称')"`
	ChannelMobile string `xorm:"varchar(300)  comment('申请人联系方式')"`

	SalesId     int    `xorm:"BIGINT(20) comment('业务员id')"`
	SalesName   string `xorm:"varchar(300)  comment('业务员名称')"`
	SalesMobile string `xorm:"varchar(300)  comment('业务员联系方式')"`

	AlipayAccount string `xorm:"varchar(300)  comment('支付宝收款账号')"`
	AlipayQrcode  string `xorm:"varchar(300)  comment('支付宝收款二维码')"`

	WepayAccount string `xorm:"varchar(300)  comment('微信收款账号')"`
	WepayQrcode  string `xorm:"varchar(300)  comment('微信收款二维码')"`

	BankName   string `xorm:"varchar(300)  comment('银行名称')"`
	Name       string `xorm:"varchar(300) comment('持卡人姓名')"`
	BankNumber string `xorm:"varchar(300) comment('银行卡号')"`
}
