package extend

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type IApply interface {
	// 物料申请列表
	List(ctx *fiber.Ctx) error
	// 物料申请详情
	Info(ctx *fiber.Ctx) error
	// 物料申请更新
	Set(ctx *fiber.Ctx) error
	// 物料申请信息删除
	Deleted(ctx *fiber.Ctx) error
}

type ApplyHandler struct {
}
