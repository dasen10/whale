package extend

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type ISales interface {
	// 业务员列表
	List(ctx *fiber.Ctx) error
	// 业务员详情
	Info(ctx *fiber.Ctx) error
	// 业务员创建
	Create(ctx *fiber.Ctx) error
	// 业务员更新 更新状态 更新字段 更新审核状态
	Set(ctx *fiber.Ctx) error
	// 业务员删除
	Deleted(ctx *fiber.Ctx) error
}

type SalesHandler struct {
}
