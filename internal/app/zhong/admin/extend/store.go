package extend

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type IStore interface {
	// 门店列表
	List(ctx *fiber.Ctx) error
	// 门店详情
	Info(ctx *fiber.Ctx) error
	// 门店创建
	Create(ctx *fiber.Ctx) error
	// 门店更新 更新状态 更新字段 更新审核状态
	Set(ctx *fiber.Ctx) error
	// 门店删除
	Deleted(ctx *fiber.Ctx) error
}

type StoreHandler struct {
}
