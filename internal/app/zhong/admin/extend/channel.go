package extend

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type IChannel interface {
	// 渠道合伙人列表
	List(ctx *fiber.Ctx) error
	// 渠道合伙人详情
	Info(ctx *fiber.Ctx) error
	// 渠道合伙人更新 更新状态 更新字段 更新审核状态
	Set(ctx *fiber.Ctx) error
	// 渠道合伙人删除
	Deleted(ctx *fiber.Ctx) error
}

type ChannelHandler struct {
}
