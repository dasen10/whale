package finance

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type IWithdraw interface {
	// 提现列表
	List(ctx *fiber.Ctx) error
	// 提现详情
	Info(ctx *fiber.Ctx) error
	// 提现信息更新
	Update(ctx *fiber.Ctx) error
	// 售后订单更新 更新状态 更新字段 更新审核状态
	Set(ctx *fiber.Ctx) error
	// 售后订单取消
	Cancel(ctx *fiber.Ctx) error
}

type WithdrawHandler struct {
}
