package business

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type IMaterial interface {
	// 物料列表
	List(ctx *fiber.Ctx) error
	// 物料详情
	Info(ctx *fiber.Ctx) error
	// 物料创建
	Create(ctx *fiber.Ctx) error
	// 物料编辑
	Update(ctx *fiber.Ctx) error
	// 物料更新
	Set(ctx *fiber.Ctx) error
	// 物料删除
	Deleted(ctx *fiber.Ctx) error
}

type MaterialHandler struct {
}
