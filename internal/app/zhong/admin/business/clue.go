package business

import "github.com/gofiber/fiber/v2"

// generate h *ClueHandler IClue
type IClue interface {
	// 线索列表
	GetList(ctx *fiber.Ctx) error
	// 线索详情
	GetInfo(ctx *fiber.Ctx) error
	// 线索更新
	Update(ctx *fiber.Ctx) error
	// 线索删除
	Deleted(ctx *fiber.Ctx) error
}

// 线索列表
func (h *ClueHandler) GetList(ctx *fiber.Ctx) error {
	panic("not implemented") // TODO: Implement
}

// 线索详情
func (h *ClueHandler) GetInfo(ctx *fiber.Ctx) error {
	panic("not implemented") // TODO: Implement
}

// 线索更新
func (h *ClueHandler) Update(ctx *fiber.Ctx) error {
	panic("not implemented") // TODO: Implement
}

// 线索删除
func (h *ClueHandler) Deleted(ctx *fiber.Ctx) error {
	panic("not implemented") // TODO: Implement
}

type ClueHandler struct{}
