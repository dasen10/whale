package business

import (
	"whale/internal/app/zhong/consts"
	"whale/internal/app/zhong/model"
	"whale/internal/repository"
	w "whale/internal/whail"
	"whale/pkg/response"
	"whale/pkg/utils"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *ThailHandler IThail
type IThail interface {
	// 服务套餐列表
	List(ctx *fiber.Ctx) error
	// 服务套餐详情
	Info(ctx *fiber.Ctx) error
	// 服务套餐创建
	Create(ctx *fiber.Ctx) error
	// 服务套餐更新
	Update(ctx *fiber.Ctx) error
	// 服务套餐状态更新
	Set(ctx *fiber.Ctx) error
	// 服务套餐删除
	Deleted(ctx *fiber.Ctx) error
}

// 服务套餐列表
func (h *ThailHandler) List(ctx *fiber.Ctx) error {
	var params consts.ThailListReq
	var list []consts.ThailListRes
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	dataBase := h.Repository.Table(&model.Serving{}).LikeSearch([]string{"name"}, params.Keyword).Bentween("create_at", params.StartTime, params.EndTime).Eq("status", params.Status)

	if err := dataBase.Paging(params.Page, params.PageSize).GetList(&list); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, list)
}

// 服务套餐详情
func (h *ThailHandler) Info(ctx *fiber.Ctx) error {
	var data consts.ThailInfoReq
	var info consts.ThailInfoRes

	if err := validate.ValidateQueryStruct(ctx, &data); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if _, err := h.Repository.Table(&model.Serving{}).Where("id = ?", data.Id).Get(&info); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, info)
}

// 服务套餐创建
func (h *ThailHandler) Create(ctx *fiber.Ctx) error {
	var data consts.ThailCreateReq

	if err := validate.ValidateJsonStruct(ctx, &data); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	var servingData model.Serving

	if err := utils.StructCopy(&data, &servingData); err != nil {
		return response.Error(ctx, 10003, err.Error())
	}
	servingData.Status = 2 // 默认下架状态
	if err := h.Repository.Table(&model.Serving{}).Insert(&servingData); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 服务套餐更新
func (h *ThailHandler) Update(ctx *fiber.Ctx) error {
	var data consts.ThailUpdateReq

	if err := validate.ValidateJsonStruct(ctx, &data); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	var servingData model.Serving

	if err := utils.StructCopy(&data, &servingData); err != nil {
		return response.Error(ctx, 10003, err.Error())
	}

	if err := h.Repository.Table(&model.Serving{}).Where("id = ?", data.Id).Update(&servingData); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

// 服务套餐状态更新
func (h *ThailHandler) Set(ctx *fiber.Ctx) error {
	var data w.SetDataReq

	if err := validate.ValidateJsonStruct(ctx, &data); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if ok := utils.ContainsKeyInSlice(data.Value, []string{"status"}); ok {
		return response.Error(ctx, 10004, "参数不合法")
	}

	if err := h.Repository.Table(&model.Serving{}).Where("id = ?", data.Id).Update(data.Value); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

// 服务套餐删除
func (h *ThailHandler) Deleted(ctx *fiber.Ctx) error {
	var data w.ByIdReq

	if err := validate.ValidateJsonStruct(ctx, &data); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.Repository.Table(&model.Serving{}).Where("id = ?", data.Id).Deleted(); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

type ThailHandler struct {
	Repository repository.IRepository `bean:"Repository"`
}
