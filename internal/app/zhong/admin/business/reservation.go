package business

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type IReservation interface {
	// 预约列表
	List(ctx *fiber.Ctx) error
	// 预约详情
	Info(ctx *fiber.Ctx) error
	// 预约更新
	Set(ctx *fiber.Ctx) error
	// 预约取消
	Cancel(ctx *fiber.Ctx) error
}

type ReservationHandler struct {
}
