package business

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type IOrder interface {
	// 订单列表
	List(ctx *fiber.Ctx) error
	// 订单详情
	Info(ctx *fiber.Ctx) error
	// 订单更新
	Set(ctx *fiber.Ctx) error
	// 订单取消
	Cancel(ctx *fiber.Ctx) error
}

type OrderHandler struct {
}
