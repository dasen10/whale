package member

import "github.com/gofiber/fiber/v2"

// generate h *MaterialHandler IMaterial
type IMember interface {
	// 会员列表
	List(ctx *fiber.Ctx) error
	// 会员详情
	Info(ctx *fiber.Ctx) error
	// 会员更新 更新状态 更新字段 更新审核状态
	Set(ctx *fiber.Ctx) error
}

type MemberHandler struct {
}
