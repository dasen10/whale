package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *BannerHandler IBanner
type IBanner interface {
	// 广告列表
	List(ctx *fiber.Ctx) error
	// 新增广告
	Create(ctx *fiber.Ctx) error
	// 更新广告
	Update(ctx *fiber.Ctx) error
	// 删除广告
	Delete(ctx *fiber.Ctx) error
	// 更新信息
	Set(ctx *fiber.Ctx) error
}

// 广告列表
func (h *BannerHandler) List(ctx *fiber.Ctx) error {
	var parasm req.BannerListReq

	if err := validate.ValidateQueryStruct(ctx, &parasm); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	list, err := h.BannerLogic.GetList(parasm)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, list)
}

// 新增广告
func (h *BannerHandler) Create(ctx *fiber.Ctx) error {
	var params req.BannerCreatreReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.BannerLogic.Create(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 更新广告
func (h *BannerHandler) Update(ctx *fiber.Ctx) error {
	var params req.BannerUpdateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.BannerLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

func (h *BannerHandler) Set(ctx *fiber.Ctx) error {
	var params req.SetDataReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.BannerLogic.Set(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 删除广告
func (h *BannerHandler) Delete(ctx *fiber.Ctx) error {
	var params req.ByIdReq

	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.BannerLogic.Deleted(params.Id); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

type BannerHandler struct {
	BannerLogic logic.IBannerLogic `bean:"BannerLogic"`
}
