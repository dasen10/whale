package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate  h *LoginHandler  ILogin
type ILogin interface {
	// 登录操作
	Login(ctx *fiber.Ctx) error
	// 退出登录
	Logout(ctx *fiber.Ctx) error
}

func (h *LoginHandler) Login(ctx *fiber.Ctx) error {
	var params req.LoginReq

	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	info, err := h.LoginLogic.LoginPost(params, ctx.IP())
	if err != nil {
		return response.Error(ctx, 1004, err.Error())
	}
	return response.Success(ctx, info)
}

func (h *LoginHandler) Logout(ctx *fiber.Ctx) error {
	return response.Success(ctx, nil)
}

type LoginHandler struct {
	LoginLogic logic.ILoginLogic `bean:"LoginLogic"`
}
