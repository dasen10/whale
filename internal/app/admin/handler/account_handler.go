package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *AccountHandler IAccount
type IAccount interface {
	// 列表
	List(ctx *fiber.Ctx) error
	// 详情
	Info(ctx *fiber.Ctx) error
	// 新增
	Create(ctx *fiber.Ctx) error
	// 更新
	Update(ctx *fiber.Ctx) error
	// 删除
	Delete(ctx *fiber.Ctx) error
	// 更新指定字段
	Set(ctx *fiber.Ctx) error
}

// 列表
func (h *AccountHandler) List(ctx *fiber.Ctx) error {
	var params req.AccountListReq
	// 验证参数
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	list, err := h.AdminLogic.GetList(params)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, list)
}

// 详情
func (h *AccountHandler) Info(ctx *fiber.Ctx) error {
	var params req.ByIdReq
	// 验证参数
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	info, err := h.AdminLogic.GetInfo(params.Id)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, info)
}

// 新增
func (h *AccountHandler) Create(ctx *fiber.Ctx) error {
	var params req.AccountCreateData

	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.AdminLogic.Create(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 更新
func (h *AccountHandler) Update(ctx *fiber.Ctx) error {
	var params req.AccountUpdateData
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.AdminLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

func (h *AccountHandler) Set(ctx *fiber.Ctx) error {
	var params req.AccountSetData

	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.AdminLogic.Set(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 删除
func (h *AccountHandler) Delete(ctx *fiber.Ctx) error {
	var parasm req.ByIdReq

	if err := validate.ValidateJsonStruct(ctx, &parasm); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.AdminLogic.Deleted(parasm.Id); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

type AccountHandler struct {
	AdminLogic logic.IAdminLogic `bean:"AdminLogic"`
}
