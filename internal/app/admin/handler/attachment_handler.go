package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *AttachmentHandler IAttachment
type IAttachment interface {
	// 读取附件列表
	List(ctx *fiber.Ctx) error
	// 删除附件
	Delete(ctx *fiber.Ctx) error
}

// 读取附件列表
func (h *AttachmentHandler) List(ctx *fiber.Ctx) error {
	var params req.AttachmentListReq
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	list, err := h.AttachmentLogic.GetList(params)

	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, list)
}

// 删除附件
func (h *AttachmentHandler) Delete(ctx *fiber.Ctx) error {
	var params req.ByIdReq

	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.AttachmentLogic.Deleted(params.Id); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

type AttachmentHandler struct {
	AttachmentLogic logic.IAttachmentLogic `bean:"AttachmentLogic"`
}
