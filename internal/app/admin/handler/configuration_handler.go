package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *ConfigurationHandler IConfiguration
type IConfiguration interface {
	// 详情
	Info(ctx *fiber.Ctx) error
	// 更新
	Update(ctx *fiber.Ctx) error
}

// 详情
func (h *ConfigurationHandler) Info(ctx *fiber.Ctx) error {
	var params req.ConfigurationInfoReq

	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	info, err := h.ConfigurationLogic.GetInfo(params.Key)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, info)
}

// 更新
func (h *ConfigurationHandler) Update(ctx *fiber.Ctx) error {
	var params req.ConfigurationUpdateReq

	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.ConfigurationLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

type ConfigurationHandler struct {
	ConfigurationLogic logic.IConfigurationLogic `bean:"ConfigurationLogic"`
}
