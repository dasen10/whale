package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *TaskHandler ITask
type ITask interface {
	// 任务列表
	List(ctx *fiber.Ctx) error
	// 任务详情
	Info(ctx *fiber.Ctx) error
	// 任务更新
	Update(ctx *fiber.Ctx) error
	// 任务创建
	Create(ctx *fiber.Ctx) error
	// 任务删除
	Delete(ctx *fiber.Ctx) error
}

// 任务列表
func (h *TaskHandler) List(ctx *fiber.Ctx) error {
	var params req.TaskListReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	list, err := h.TaskLogic.GetList(params)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, list)
}

// 任务详情
func (h *TaskHandler) Info(ctx *fiber.Ctx) error {
	var params req.ByIdReq
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	info, err := h.TaskLogic.GetInfo(params.Id)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, info)
}

// 任务更新
func (h *TaskHandler) Update(ctx *fiber.Ctx) error {
	var params req.TaskUpdateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.TaskLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 任务创建
func (h *TaskHandler) Create(ctx *fiber.Ctx) error {
	var params req.TaskCreateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.TaskLogic.Create(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 任务删除
func (h *TaskHandler) Delete(ctx *fiber.Ctx) error {
	var params req.ByIdReq
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.TaskLogic.Deleted(params.Id); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

type TaskHandler struct {
	TaskLogic logic.ITaskLogic `bean:"TaskLogic"`
}
