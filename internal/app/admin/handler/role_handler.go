package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *RoleHandler IRole
type IRole interface {
	// 权限列表
	List(ctx *fiber.Ctx) error
	// 权限详情
	Info(ctx *fiber.Ctx) error
	// 权限更新
	Update(ctx *fiber.Ctx) error
	// 权限创建
	Create(ctx *fiber.Ctx) error
	// 权限删除
	Delete(ctx *fiber.Ctx) error
}

// 权限列表
func (h *RoleHandler) List(ctx *fiber.Ctx) error {
	var params req.RoleListReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	list, err := h.RoleLogic.GetList(params)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, list)
}

// 权限详情
func (h *RoleHandler) Info(ctx *fiber.Ctx) error {
	var params req.ByIdReq

	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	info, err := h.RoleLogic.GetInfo(params.Id)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, info)
}

// 权限更新
func (h *RoleHandler) Update(ctx *fiber.Ctx) error {
	var params req.RoleUpdatgeReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.RoleLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 权限创建
func (h *RoleHandler) Create(ctx *fiber.Ctx) error {
	var params req.RoleCreateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.RoleLogic.Create(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 权限删除
func (h *RoleHandler) Delete(ctx *fiber.Ctx) error {
	var params req.ByIdReq
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.RoleLogic.Deleted(params.Id); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

type RoleHandler struct {
	RoleLogic logic.IRoleLogic `bean:"RoleLogic"`
}
