package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *DataHandler IData
type IData interface {
	// 配置列表
	List(ctx *fiber.Ctx) error
	// 配置详情
	Info(ctx *fiber.Ctx) error
	// 配置更新
	Update(ctx *fiber.Ctx) error
	// 配置创建
	Create(ctx *fiber.Ctx) error
	// 配置删除
	Delete(ctx *fiber.Ctx) error
}

// 配置列表
func (h *DataHandler) List(ctx *fiber.Ctx) error {
	var params req.DataListReq

	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	list, err := h.DataLogic.GetList(params)

	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, list)
}

// 配置详情
func (h *DataHandler) Info(ctx *fiber.Ctx) error {
	var params req.ByIdReq

	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	info, err := h.DataLogic.GetInfo(params.Id)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, info)
}

// 配置更新
func (h *DataHandler) Update(ctx *fiber.Ctx) error {
	var params req.DataUpdateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.DataLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

// 配置创建
func (h *DataHandler) Create(ctx *fiber.Ctx) error {
	var params req.DataCreateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.DataLogic.Create(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

// 配置删除
func (h *DataHandler) Delete(ctx *fiber.Ctx) error {
	var params req.ByIdReq

	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.DataLogic.Deleted(params.Id); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

type DataHandler struct {
	DataLogic logic.IDataLogic `bean:"DataLogic"`
}
