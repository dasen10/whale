package handler

import (
	"whale/internal/app/admin/logic"
	"whale/pkg/response"

	"github.com/gofiber/fiber/v2"
)

// generate h *UploadHandler IUpload
type IUpload interface {
	// 图片上传
	Image(ctx *fiber.Ctx) error
	// 文件上传
	File(ctx *fiber.Ctx) error
}

// 图片上传
func (h *UploadHandler) Image(ctx *fiber.Ctx) error {
	info, err := h.UploadLogic.UploadImage(ctx)
	if err != nil {
		return response.Error(ctx, 1004, err.Error())
	}
	return response.Success(ctx, info)
}

// 文件上传
func (h *UploadHandler) File(ctx *fiber.Ctx) error {
	panic("not implemented") // TODO: Implement
}

type UploadHandler struct {
	UploadLogic logic.IUploadLogic `bean:"UploadLogic"`
}
