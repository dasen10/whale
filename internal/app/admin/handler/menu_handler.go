package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *MenuHandler IMenu
type IMenu interface {
	// 菜单列表
	List(ctx *fiber.Ctx) error
	// 菜单详情
	Info(ctx *fiber.Ctx) error
	// 菜单更新
	Update(ctx *fiber.Ctx) error
	// 菜单创建
	Create(ctx *fiber.Ctx) error
	// 菜单删除
	Delete(ctx *fiber.Ctx) error
}

// 菜单列表
func (h *MenuHandler) List(ctx *fiber.Ctx) error {
	var params req.PagingReq

	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	list, err := h.MenuLogic.GetList(params)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, list)
}

// 菜单详情
func (h *MenuHandler) Info(ctx *fiber.Ctx) error {
	var params req.ByIdReq
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	info, err := h.MenuLogic.GetInfo(params.Id)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, info)
}

// 菜单更新
func (h *MenuHandler) Update(ctx *fiber.Ctx) error {
	var params req.MenuUpdateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.MenuLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 菜单创建
func (h *MenuHandler) Create(ctx *fiber.Ctx) error {
	var params req.MenuCreateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	if err := h.MenuLogic.Create(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 菜单删除
func (h *MenuHandler) Delete(ctx *fiber.Ctx) error {
	var params req.ByIdReq
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.MenuLogic.Deleted(params.Id); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

type MenuHandler struct {
	MenuLogic logic.IMenuLogic `bean:"MenuLogic"`
}
