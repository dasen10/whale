package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *DataKeyHandler IDataKey
type IDataKey interface {
	// 配置key列表
	List(ctx *fiber.Ctx) error
	// 配置key详情
	Info(ctx *fiber.Ctx) error
	// 配置key更新
	Update(ctx *fiber.Ctx) error
	// 配置key创建
	Create(ctx *fiber.Ctx) error
	// 配置key删除
	Delete(ctx *fiber.Ctx) error
}

// 配置key列表
func (h *DataKeyHandler) List(ctx *fiber.Ctx) error {
	var params req.DataKeyListReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	list, err := h.DataKeyLogic.GetList(params)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, list)
}

// 配置key详情
func (h *DataKeyHandler) Info(ctx *fiber.Ctx) error {
	var params req.ByIdReq
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	info, err := h.DataKeyLogic.GetInfo(params.Id)
	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, info)
}

// 配置key更新
func (h *DataKeyHandler) Update(ctx *fiber.Ctx) error {
	var params req.DataKeyUpdateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.DataKeyLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 配置key创建
func (h *DataKeyHandler) Create(ctx *fiber.Ctx) error {
	var params req.DataKeyCreateReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.DataKeyLogic.Create(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

// 配置key删除
func (h *DataKeyHandler) Delete(ctx *fiber.Ctx) error {
	var params req.ByIdReq

	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.DataKeyLogic.Deleted(params.Id); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, nil)
}

type DataKeyHandler struct {
	DataKeyLogic logic.IDataKeyLogic `bean:"DataKeyLogic"`
}
