package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *OplogsHandler IOplogs
type IOplogs interface {
	// 日志列表
	List(ctx *fiber.Ctx) error
	// 日志删除
	Delete(ctx *fiber.Ctx) error
}

// 日志列表
func (h *OplogsHandler) List(ctx *fiber.Ctx) error {
	var params req.OplogsReq

	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	// list, err :=
	return response.Success(ctx, nil)
}

// 日志删除
func (h *OplogsHandler) Delete(ctx *fiber.Ctx) error {
	var params req.ByIdReq
	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}
	return response.Success(ctx, nil)
}

type OplogsHandler struct {
}
