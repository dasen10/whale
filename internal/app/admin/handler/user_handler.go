package handler

import (
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
	"whale/pkg/response"

	"github.com/gofiber/fiber/v2"
)

// generate h *UserHandler IUser
type IUser interface {
	UserInfo(ctx *fiber.Ctx) error
}

func (h *UserHandler) UserInfo(ctx *fiber.Ctx) error {
	adminId := ctx.Locals("admin_id").(int)
	var userInfo res.UserInfoRes
	if _, err := h.Repository.Table(&model.Account{}).Where("id = ?", adminId).Get(&userInfo); err != nil {
		return response.Error(ctx, 10004, "用户信息查询失败")
	}
	return response.Success(ctx, userInfo)
}

type UserHandler struct {
	Repository repository.IRepository `bean:"Repository"`
}
