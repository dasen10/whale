package handler

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/logic"
	"whale/pkg/response"
	"whale/pkg/validate"

	"github.com/gofiber/fiber/v2"
)

// generate h *PageHandler IPage
type IPage interface {
	// 日志列表
	Info(ctx *fiber.Ctx) error
	// 日志删除
	Update(ctx *fiber.Ctx) error
}

// 日志列表
func (h *PageHandler) Info(ctx *fiber.Ctx) error {
	var params req.PageInfoReq
	if err := validate.ValidateQueryStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	info, err := h.PageLogic.GetInfo(params.Key)

	if err != nil {
		return response.Error(ctx, 10004, err.Error())
	}
	return response.Success(ctx, info)
}

// 日志删除
func (h *PageHandler) Update(ctx *fiber.Ctx) error {
	var params req.PageUpdateReq

	if err := validate.ValidateJsonStruct(ctx, &params); err != nil {
		return response.Error(ctx, 10001, err.Error())
	}

	if err := h.PageLogic.Update(params); err != nil {
		return response.Error(ctx, 10004, err.Error())
	}

	return response.Success(ctx, nil)
}

type PageHandler struct {
	PageLogic logic.IPageLogic `bean:"PageLogic"`
}
