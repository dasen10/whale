package req

// 查询列表
type AttachmentListReq struct {
	PagingReq
	SearchKeywordReq
}
