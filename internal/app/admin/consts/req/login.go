package req

type LoginReq struct {
	Account  string `validate:"required" json:"account,omitempty"`
	Password string `validate:"required" json:"password,omitempty"`
}
