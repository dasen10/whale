package req

import "whale/internal/models"

// 广告列表
type BannerListReq struct {
	Key string `validate:"required" json:"key,omitempty"`
	PagingReq
}

// 创建广告
type BannerCreatreReq struct {
	Name     string          `validate:"required" json:"name,omitempty"`
	Desc     string          `validate:"-" json:"desc,omitempty"`
	Image    string          `validate:"required" json:"image,omitempty"`
	Key      string          `validate:"required" json:"key,omitempty"`
	Url      string          `validate:"-" json:"url,omitempty"`
	Type     int             `validate:"required" json:"type,omitempty"`
	Path     string          `validate:"-" json:"path,omitempty"`
	Weapp    string          `validate:"-" json:"weapp,omitempty"`
	CreateAt models.XormTime `validate:"required" json:"create_at,omitempty"`
}

// 更新广告
type BannerUpdateReq struct {
	ByIdReq
	BannerCreatreReq
	UpdateAt models.XormTime `validate:"required" json:"update_at,omitempty"`
}
