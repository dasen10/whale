package req

type ConfigurationInfoReq struct {
	Key string `validate:"required" json:"key,omitempty"`
}

type ConfigurationUpdateReq struct {
	Id int `validate:"required" json:"id,omitempty"`
}
