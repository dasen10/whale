package req

// 公共请求参数
type PagingReq struct {
	Page     int `validate:"required|integer" query:"page" json:"page,omitempty"`
	PageSize int `validate:"required|integer" query:"page_size" json:"page_size,omitempty"`
}

// 公共请求参数
type SetDataReq struct {
	Id    int                    `validate:"required" json:"id,omitempty"`
	Value map[string]interface{} `validate:"required" json:"value,omitempty"`
}

// 公共请求参数
type ByIdReq struct {
	Id int `validate:"required" json:"id,omitempty"`
}

// 公共请求参数
type SearchKeywordReq struct {
	Keyword   string `json:"keyword,omitempty" validate:"-"`
	StartTime string `json:"start_time,omitempty" validate:"-"`
	EndTime   string `validate:"-" json:"end_time,omitempty"`
	Status    int    `validate:"-" json:"status,omitempty"`
}

// 账户列表
type AccountListReq struct {
	PagingReq
	SearchKeywordReq
}

// 创建账号
type AccountCreateData struct {
	Account   string `validate:"required" json:"account,omitempty"`
	Password  string `validate:"-" json:"password,omitempty"`
	NickName  string `validate:"required" json:"nick_name,omitempty"`
	Headimg   string `validate:"-" json:"headimg,omitempty"`
	Authorize string `validate:"required" json:"authorize,omitempty"`
	Mobile    string `validate:"required" json:"mobile,omitempty"`
	Describe  string `validate:"-" json:"describe,omitempty"`
}

// 更新账号信息
type AccountUpdateData struct {
	Id int `validate:"required" json:"id,omitempty"`
	AccountCreateData
}

type AccountSetData struct {
	Id    int                    `validate:"required" json:"id,omitempty"`
	Value map[string]interface{} `validate:"required" json:"value,omitempty"`
}
