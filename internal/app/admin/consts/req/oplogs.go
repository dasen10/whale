package req

// 日志列表请求
type OplogsReq struct {
	PagingReq
	SearchKeywordReq
}
