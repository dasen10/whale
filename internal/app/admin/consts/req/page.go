package req

type PageInfoReq struct {
	Key string `validate:"required" json:"key,omitempty"`
}

type PageUpdateReq struct {
	ByIdReq
}
