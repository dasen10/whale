package req

type DataListReq struct {
	Key string `validate:"required" json:"key,omitempty"`
	PagingReq
	SearchKeywordReq
}

type DataCreateReq struct {
}

type DataUpdateReq struct {
	ByIdReq
}
