package req

type TaskListReq struct {
	PagingReq
	SearchKeywordReq
}

type TaskCreateReq struct {
}

type TaskUpdateReq struct {
	ByIdReq
}
