package res

import "whale/internal/models"

// 返回列表
type BannerListRes struct {
	Id       int             `json:"id,omitempty"`
	Sort     int             `json:"sort,omitempty"`
	Name     string          `json:"name,omitempty"`
	Desc     string          `json:"desc,omitempty"`
	Image    string          `json:"image,omitempty"`
	Type     string          `json:"type,omitempty"`
	Path     string          `json:"path,omitempty"`
	Url      string          `json:"url,omitempty"`
	Weapp    string          `json:"weapp,omitempty"`
	Status   int             `json:"status,omitempty"`
	CreateAt models.XormTime `json:"create_at,omitempty"`
}

type BannerInfoRes struct {
}
