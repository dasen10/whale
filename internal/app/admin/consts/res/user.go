package res

type UserInfoRes struct {
	NickName  string `json:"admin_name"`
	Headimg   string `json:"headimg"`
	Authorize string `json:"role"`
}
