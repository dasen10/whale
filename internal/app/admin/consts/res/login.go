package res

type LoginRes struct {
	Token     string `json:"token,omitempty"`
	AdminName string `json:"admin_name,omitempty"`
	Headimg   string `json:"headimg,omitempty"`
	Auth      string `json:"auth,omitempty"`
}
