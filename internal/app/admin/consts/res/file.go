package res

type UploadImageRes struct {
	ImageSrc string `json:"image_src,omitempty"`
}
