package res

import "whale/internal/models"

// 日志列表返回
type OplogsListRes struct {
	Id        int             `json:"id,omitempty"`
	CreateAt  models.XormTime `json:"create_at,omitempty"`
	Node      string          `json:"node,omitempty"`
	Geoip     string          `json:"geoip,omitempty"`
	Action    string          `json:"action,omitempty"`
	Content   string          `json:"content,omitempty"`
	AdminName string          `json:"admin_name,omitempty"`
	AdminId   int             `json:"admin_id,omitempty"`
}
