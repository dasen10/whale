package res

import "whale/internal/models"

type AccountRecodRes struct {
	Id        int             `json:"id,omitempty"`
	Mobile    string          `json:"mobile,omitempty"`
	Account   string          `json:"account,omitempty"`
	NickName  string          `json:"nick_name,omitempty"`
	Headimg   string          `json:"headimg,omitempty"`
	Authorize string          `json:"authorize"`
	LoginIp   string          `json:"login_ip"`
	LoginNum  int             `json:"login_num"`
	LoginAt   models.XormTime `json:"login_at"`
	Describe  string          `json:"describe"`
	Status    int             `json:"status"`
	CreateAt  models.XormTime `json:"create_at"`
}
