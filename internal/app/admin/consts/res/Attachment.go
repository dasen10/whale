package res

import "whale/internal/models"

type AttachmentListRes struct {
	Id       int             `json:"id,omitempty"`
	CreateAt models.XormTime `json:"create_at,omitempty"`
	Type     string          `json:"type,omitempty"`
	Hash     string          `json:"hash,omitempty"`
	Name     string          `json:"name,omitempty"`
	Xext     string          `json:"xext,omitempty"`
	Xurl     string          `json:"xurl,omitempty"`
	Xkey     string          `json:"xkey,omitempty"`
	Mime     string          `json:"mime,omitempty"`
	Size     int             `json:"size,omitempty"`
	Uuid     string          `json:"uuid,omitempty"`
	Isfast   int             `json:"isfast,omitempty"`
	Issafe   int             `json:"issafe,omitempty"`
	Status   int             `json:"status,omitempty"`
}
