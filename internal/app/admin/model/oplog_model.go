package model

import "whale/internal/models"

type Oplog struct {
	models.BaseModel `xorm:"extends"`
	Node             string `xorm:"varchar(200) null comment('当前操作节点')"`
	Geoip            string `xorm:"varchar(100) null comment('操作者IP地址')"`
	Action           string `xorm:"varchar(200) null comment('操作行为名称')"`
	Content          string `xorm:"text  null comment('操作内容描述')"`
	AdminName        string `xorm:"varchar(200) null comment('操作人')"`
	AdminId          int    `xorm:"null comment('关联操作人id')"`
}
