package model

import "whale/internal/models"

// 权限表
type AdminAuth struct {
	Id       int             `xorm:"pk autoincr comment('数据id')"`
	Title    string          `xorm:"varchar(200) notnull comment('权限名称')"`
	Utype    string          `xorm:"varchar(100) notnull comment('权限编码')"`
	Desc     string          `xorm:"varchar(500) null comment('权限简介')"`
	Node     string          `xorm:"text  null comment('权限节点')"`
	Sort     int             `xorm:"tinyint(4) default(0) comment('排序值')"`
	CreateAt models.XormTime `xorm:"created"`
}
