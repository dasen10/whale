package model

import "whale/internal/models"

// 广告管理 广告位只能通过代码设置
type Banner struct {
	models.BaseModel `xorm:"extends"`
	SpaceKey         string `xorm:"varchar(100) comment('广告位key')"`
	SpaceName        string `xorm:"varchar(100) comment('广告位名称')"`
	Name             string `xorm:"varchar(100) comment('广告名称')"`
	Image            string `xorm:"varchar(500) comment('广告地址')"`
	Type             int    `xorm:"tinyint(3) default(1) comment('广告类型 1=图片 2=视频')"`
	Desc             string `xorm:"varchar(500) comment('广告描述')"`
	LinkType         int    `xorm:"tinyint(3) default(1) comment('跳转类型 1=链接 2=页面')"`
	Link             string `xorm:"varchar(500) comment('跳转链接')"`
	Path             string `xorm:"varchar(500) comment('小程序页面')"`
	Sort             int    `xorm:"tinyint(4) default(0) comment('排序至')"`
	Status           int    `xorm:"tinyint(4) default(1) comment('广告状态 1=正常 2=禁用')"`
}
