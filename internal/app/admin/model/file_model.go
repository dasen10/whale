package model

import "whale/internal/models"

type File struct {
	models.BaseModel `xorm:"extends"`
	Type             string `xorm:"varchar(20) index notnull comment('上传文件类型')"`
	Hash             string `xorm:"varchar(50) index notnull comment('文件hash值')"`
	Name             string `xorm:"varchar(200) notnull comment('文件名称')"`
	Xext             string `xorm:"varchar(100) index notnull comment('文件后缀名')"`
	Xurl             string `xorm:"varchar(500) null comment('访问链接')"`
	Xkey             string `xorm:"varchar(500) null comment('文件路径')"`
	Mime             string `xorm:"varchar(100) null comment('文件类型')"`
	Size             int    `xorm:"BIGINT(20) default(0) comment('文件大小')"`
	Uuid             int    `xorm:"BIGINT(20) index default(0) comment('用户编号')"`
	Isfast           int    `xorm:"tinyint(3) index default(0) comment('是否秒传')"`
	Issafe           int    `xorm:"tinyint(3) index default(0) comment('安全模式')"`
	Status           int    `xorm:"tinyint(3) index default(0) comment('上传状态(1悬空,2落地)')"`
}
