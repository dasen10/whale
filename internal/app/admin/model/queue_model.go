package model

import "whale/internal/models"

type QueueStatus int

const (
	_ QueueStatus = iota
	// 新任务
	NewQueueStatus
	// 进行中
	OngoingQueueStatus
	// 成功
	SuccessQueueStatus
	// 失败
	FailureQueueStatus
)

type Rscript int

const (
	_ Rscript = iota
	// 单例任务
	SingletonRscript
	// 多例任务
	MultitonRscript
)

type Queue struct {
	models.BaseModel `xorm:"extends"`
	Code             string `xorm:"varchar(200) notnull comment('任务编号')"`
	Title            string `xorm:"varchar(120) notnull comment('任务标题')"`
	Command          string `xorm:"varchar(500) notnull comment('执行指令')"`
	ExecPid          int    `xorm:"BIGINT(20) null comment('执行进程pid')"`
	ExecData         string `xorm:"text null comment('执行参数')"`
	ExecTime         int    `xorm:"BIGINT(20) null comment('执行时间')"`
	ExecDesc         string `xorm:"varchar(500) null comment('执行描述')"`
	EnterTime        int    `xorm:"BIGINT(20) null comment('开始时间')"`
	OuterTime        int    `xorm:"BIGINT(20) null comment('结束时间')"`
	LoopsTime        int    `xorm:"BIGINT(20) default(0) comment('循环时间')"`
	Attempts         int    `xorm:"BIGINT(20) default(0) comment('执行次数')"`
	Rscript          int    `xorm:"tinyint(3) default(1) comment('任务类型(0单例,1多例)')"`
	Status           int    `xorm:"tinyint(3) default(1) comment('任务状态(1新任务,2处理中,3成功,4失败)')"`
}
