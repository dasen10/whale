package model

// 配置信息表
type Config struct {
	Id    int    `xorm:"pk  autoincr comment('数据id')"`
	Key   string `xorm:"varchar(100) unique index comment('数据key')"`
	Name  string `xorm:"varchar(100) unique index comment('数据名称')"`
	Value string `xorm:"text  null comment('数据json存储')"`
}
