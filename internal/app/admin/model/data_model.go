package model

import "whale/internal/models"

// 文章分类
type DataKey struct {
	Id     int    `xorm:"pk  autoincr comment('数据id')"`
	Key    string `xorm:"varchar(100) unique index comment('数据key')"`
	Name   string `xorm:"varchar(100) comment('数据名称')"`
	Sort   int    `xorm:"tinyint(4) default(0) comment('排序值')"`
	Type   int    `xorm:"tinyint(4) default(1) comment('内容类型 1=富文本 2=纯文本')"`
	Status int    `xorm:"tinyint(4) default(1) comment('状态 1=不可删除 2=可删除')"`
}

// 文章内容
type Data struct {
	models.BaseModel `xorm:"extends"`
	Title            string `xorm:"varchar(100)  comment('数据key')"`
	Sort             int    `xorm:"tinyint(4) default(0) comment('排序值')"`
	Excerpt          string `xorm:"varchar(100)  comment('摘要简介')"`
	Image            string `xorm:"varchar(500)  comment('内容封面')"`
	Status           int    `xorm:"tinyint(4) default(1) comment('状态 1=正常 2=禁用')"`
	Content          string `xorm:"text comment('文章内容')"`
}
