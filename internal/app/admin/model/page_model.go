package model

import "whale/internal/models"

//
type PageBlock struct {
	models.BaseModel `xorm:"extends"`
	Code             string `xorm:"varchar(200) notnull index comment('数据Key')"`
	Name             string `xorm:"varchar(200) notnull index comment('数据名称')"`
	Content          string `xorm:"text null comment('数据块内容')"`
	Sort             int    `xorm:"tinyint(3) default(0) comment('排序值')"`
	AdminId          int    `xorm:"BIGINT(20) notnull index comment('数据操作管理员id')"`
	AdminName        string `xorm:"varchar(100) notnull  comment('数据操作管理员名称')"`
}
