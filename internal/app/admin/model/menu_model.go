package model

import "whale/internal/models"

type Menu struct {
	models.BaseModel `xorm:"extends"`
	Type             int
	PreviousMenuId   int `xorm:""`
	PreviousMenuName string
	Name             string
	Path             string
	Arguments        string
	Node             string
	IconName         string
	Status           int
	Sort             int
}
