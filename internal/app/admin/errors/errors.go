package errors

import (
	"errors"
)

// 文件异常
func FileNotError() error {
	return errors.New("FileNotError:0xFF1040FF")
}

// 参数合法性验证
func ContainsKeyError() error {
	return errors.New("ContainsKeyError:0xFF1030FF")
}

// 结构体copy异常
func StructCopyError() error {
	return errors.New("StructCopyError:0xFF1020FF")
}

// 数据验证失败错误提示
func ValidateError() error {
	return errors.New("ValidateError:0xFF1010FF")
}

// 数据库读取失败
func DBReadError() error {
	return errors.New("DBReadError:0xFF1020CF")
}

// 数据库存储失败
func DBStoreError() error {
	return errors.New("DBStoreError:0xFF10301F")
}

// 数据不合法
func DataLegalError() error {
	return errors.New("DataLegalError:0xFF10301F")
}
