package admin

import (
	"whale/internal/app/admin/handler"
	"whale/internal/app/admin/logic"
	"whale/internal/middleware"
	"whale/pkg/bean"

	"github.com/gofiber/fiber/v2"
)

type IAdminRouter interface {
	AdminRouterInit(app *fiber.App)
}

type AdminRouter struct {
	LoginHandler         handler.ILogin         `bean:"LoginHandler"`
	AccountHandler       handler.IAccount       `bean:"AccountHandler"`
	OplogsHandler        handler.IOplogs        `bean:"OplogsHandler"`
	AttachmentHandler    handler.IAttachment    `bean:"AttachmentHandler"`
	BannerHandler        handler.IBanner        `bean:"BannerHandler"`
	ConfigurationHandler handler.IConfiguration `bean:"ConfigurationHandler"`
	DataHandler          handler.IData          `bean:"DataHandler"`
	DataKeyHandler       handler.IDataKey       `bean:"DataKeyHandler"`
	MenuHandler          handler.IMenu          `bean:"MenuHandler"`
	PageHandler          handler.IPage          `bean:"PageHandler"`
	RoleHandler          handler.IRole          `bean:"RoleHandler"`
	TaskHandler          handler.ITask          `bean:"TaskHandler"`
	UploadHandler        handler.IUpload        `bean:"UploadHandler"`
	UserHandler          handler.IUser          `bean:"UserHandler"`
}

// 路由生成器
func (admin *AdminRouter) AdminRouterInit(app *fiber.App) {

	loginApi := app.Group("/admin/v1/api")
	{
		loginApi.Post("/login", admin.LoginHandler.Login)
		loginApi.Post("/logout", admin.LoginHandler.Logout)
	}

	adminApi := app.Group("/admin/v1/api")
	{
		// 验证token
		adminApi.Use(middleware.AdminTokenMiddeware)

		// 首页统计
		adminApi.Get("/content-data", admin.LoginHandler.Logout)
		adminApi.Get("/popular/list", admin.LoginHandler.Logout)

		adminApi.Post("/user/info", admin.UserHandler.UserInfo)

		adminApi.Post("/upload/image", admin.UploadHandler.Image)
		// 账号管理
		adminApi.Get("/system/account.list", admin.AccountHandler.List)
		adminApi.Get("/system/account.info", admin.AccountHandler.Info)
		adminApi.Post("/system/account.update", admin.AccountHandler.Update)
		adminApi.Post("/system/account.delete", admin.AccountHandler.Delete)
		adminApi.Post("/system/account.create", admin.AccountHandler.Create)
		adminApi.Post("/system/account.set", admin.AccountHandler.Set)

		// 系统日志
		adminApi.Get("/system/oplogs.list", admin.OplogsHandler.List)
		adminApi.Post("/system/oplogs.delete", admin.OplogsHandler.Delete)

		// 菜单管理
		adminApi.Get("/system/menu.list", admin.MenuHandler.List)
		adminApi.Get("/system/menu.info", admin.MenuHandler.Info)
		adminApi.Post("/system/menu.create", admin.MenuHandler.Create)
		adminApi.Post("/system/menu.update", admin.MenuHandler.Update)
		adminApi.Post("/system/menu.delete", admin.MenuHandler.Delete)

		// 角色权限
		adminApi.Get("/system/role.list", admin.RoleHandler.List)
		adminApi.Get("/system/role.info", admin.RoleHandler.Info)
		adminApi.Post("/system/role.update", admin.RoleHandler.Update)
		adminApi.Post("/system/role.create", admin.RoleHandler.Create)
		adminApi.Post("/system/role.delete", admin.RoleHandler.Delete)

		// 系统任务
		adminApi.Get("/system/task.list", admin.TaskHandler.List)
		adminApi.Get("/system/task.info", admin.TaskHandler.Info)
		adminApi.Post("/system/task.create", admin.TaskHandler.Create)
		adminApi.Post("/system/task.update", admin.TaskHandler.Update)
		adminApi.Post("/system/task.delete", admin.TaskHandler.Delete)

		// 字典数据 key
		adminApi.Get("/datacenter/data.key.list", admin.DataKeyHandler.List)
		adminApi.Post("/datacenter/data.key.create", admin.DataKeyHandler.Create)
		adminApi.Post("/datacenter/data.key.update", admin.DataKeyHandler.Update)
		adminApi.Post("/datacenter/data.key.delete", admin.DataKeyHandler.Delete)
		// 字典数据 value
		adminApi.Get("/datacenter/data.list", admin.DataHandler.List)
		adminApi.Post("/datacenter/data.create", admin.DataHandler.Create)
		adminApi.Post("/datacenter/data.update", admin.DataHandler.Update)
		adminApi.Post("/datacenter/data.delete", admin.DataHandler.Delete)

		// attachment 附件管理
		adminApi.Get("/datacenter/attachment.list", admin.AttachmentHandler.List)
		adminApi.Post("/datacenter/attachment.delete", admin.AttachmentHandler.Delete)

		// 配置管理
		adminApi.Get("/datacenter/configuration.info", admin.ConfigurationHandler.Info)
		adminApi.Post("/datacenter/configuration.update", admin.ConfigurationHandler.Update)

		// 配置管理
		adminApi.Get("/datacenter/banner.list", admin.BannerHandler.List)
		adminApi.Post("/datacenter/banner.create", admin.BannerHandler.Create)
		adminApi.Post("/datacenter/banner.update", admin.BannerHandler.Update)
		adminApi.Post("/datacenter/banner.delete", admin.BannerHandler.Delete)

		// 单页管理
		adminApi.Get("/datacenter/page.info", admin.PageHandler.Info)
		adminApi.Post("/datacenter/page.update", admin.PageHandler.Update)
	}

}

// 依赖自动注入
func NewAdminRouter(beanContainer *bean.BeanContainer) *AdminRouter {
	beanContainer.SetSingleton("LoginLogic", &logic.LoginLogic{})
	beanContainer.SetSingleton("AdminLogic", &logic.AdminLogic{})
	beanContainer.SetSingleton("OplogsLogic", &logic.OplogsLogic{})
	beanContainer.SetSingleton("AttachmentLogic", &logic.AttachmentLogic{})
	beanContainer.SetSingleton("BannerLogic", &logic.BannerLogic{})
	beanContainer.SetSingleton("ConfigurationLogic", &logic.ConfigurationLogic{})
	beanContainer.SetSingleton("DataKeyLogic", &logic.DataKeyLogic{})
	beanContainer.SetSingleton("DataLogic", &logic.DataLogic{})
	beanContainer.SetSingleton("MenuLogic", &logic.MenuLogic{})
	beanContainer.SetSingleton("PageLogic", &logic.PageLogic{})
	beanContainer.SetSingleton("RoleLogic", &logic.RoleLogic{})
	beanContainer.SetSingleton("TaskLogic", &logic.TaskLogic{})
	beanContainer.SetSingleton("UploadLogic", &logic.UploadLogic{})

	beanContainer.SetSingleton("LoginHandler", &handler.LoginHandler{})
	beanContainer.SetSingleton("AccountHandler", &handler.AccountHandler{})
	beanContainer.SetSingleton("OplogsHandler", &handler.OplogsHandler{})
	beanContainer.SetSingleton("AttachmentHandler", &handler.AttachmentHandler{})
	beanContainer.SetSingleton("BannerHandler", &handler.BannerHandler{})
	beanContainer.SetSingleton("ConfigurationHandler", &handler.ConfigurationHandler{})
	beanContainer.SetSingleton("DataHandler", &handler.DataHandler{})
	beanContainer.SetSingleton("DataKeyHandler", &handler.DataKeyHandler{})
	beanContainer.SetSingleton("MenuHandler", &handler.MenuHandler{})
	beanContainer.SetSingleton("PageHandler", &handler.PageHandler{})
	beanContainer.SetSingleton("RoleHandler", &handler.RoleHandler{})
	beanContainer.SetSingleton("TaskHandler", &handler.TaskHandler{})
	beanContainer.SetSingleton("UploadHandler", &handler.UploadHandler{})
	beanContainer.SetSingleton("UserHandler", &handler.UserHandler{})
	return &AdminRouter{}
}
