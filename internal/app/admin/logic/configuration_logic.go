package logic

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/errors"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
	"whale/pkg/utils"
)

// generate logic *ConfigurationLogic IConfigurationLogic
type IConfigurationLogic interface {
	// 配置详情
	GetInfo(key string) (interface{}, error)
	// 更新配置
	Update(data req.ConfigurationUpdateReq) error
}

// 配置详情
func (logic *ConfigurationLogic) GetInfo(key string) (interface{}, error) {
	var info res.ConfigurationInfoRes
	if _, err := logic.Repository.Table(&model.Config{}).Where("key = ?", key).Get(&info); err != nil {
		return nil, errors.DBReadError()
	}
	return info, nil
}

// 更新配置
func (logic *ConfigurationLogic) Update(data req.ConfigurationUpdateReq) error {
	var configData model.Config
	if err := utils.StructCopy(&configData, &data); err != nil {
		return errors.DataLegalError()
	}
	return logic.Repository.Table(&model.Config{}).Where("id = ?", data.Id).Update(&configData)
}

type ConfigurationLogic struct {
	Repository repository.IRepository `bean:"Repository"`
}
