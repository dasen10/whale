package logic

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/errors"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
)

// generate logic *OplogsLogic IOplogsLogic
type IOplogsLogic interface {
	// 查询日志列表
	GetList(data req.OplogsReq) (interface{}, error)
	// 删除日志
	Deleted(id int) error
}

// 查询日志列表
func (logic *OplogsLogic) GetList(data req.OplogsReq) (interface{}, error) {
	var list []res.OplogsListRes

	dataBase := logic.Repository.Table(&model.Oplog{}).LikeSearch([]string{"action", "content"}, data.Keyword).Bentween("create_at", data.StartTime, data.EndTime).Eq("status", data.Status)

	if err := dataBase.OrderBy("id desc").GetList(&list); err != nil {
		return nil, errors.DBReadError()
	}
	return list, nil
}

// 删除日志
func (logic *OplogsLogic) Deleted(id int) error {
	if err := logic.Repository.Table(&model.Oplog{}).Where("id = ?", id).Deleted(); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

type OplogsLogic struct {
	Repository repository.IRepository `bean:"Repository"`
}
