package logic

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/errors"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
	"whale/pkg/utils"
)

// logic *DataLogic IDataLogic
type IDataLogic interface {
	// 广告列表
	GetList(data req.DataListReq) (interface{}, error)
	// 广告详情
	GetInfo(id int) (interface{}, error)
	// 创建广告
	Create(data req.DataCreateReq) error
	// 更新广告
	Update(data req.DataUpdateReq) error
	// 删除广告
	Deleted(id int) error
	// 更新数据
	Set(data req.SetDataReq) error
}

// 广告列表
func (logic *DataLogic) GetList(data req.DataListReq) (interface{}, error) {
	var list []res.DataListRes

	dataBase := logic.Repository.Table(&model.Data{}).LikeSearch([]string{"title"}, data.Keyword).Bentween("create_at", data.StartTime, data.EndTime).Eq("status", data.Status)

	if err := dataBase.Where("key = ?", data.Key).OrderBy("sort desc").GetList(&list); err != nil {
		return nil, errors.DBReadError()
	}

	return list, nil
}

// 广告详情
func (logic *DataLogic) GetInfo(id int) (interface{}, error) {
	var info res.DataInfoRes

	if _, err := logic.Repository.Table(&model.Data{}).Where("id = ?", id).Get(&info); err != nil {
		return nil, errors.DBReadError()
	}
	return info, nil
}

// 创建广告
func (logic *DataLogic) Create(data req.DataCreateReq) error {
	var dataData model.Data
	if err := utils.StructCopy(&dataData, &data); err != nil {
		return errors.DataLegalError()
	}
	if err := logic.Repository.Table(&model.Data{}).Insert(&dataData); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

// 更新广告
func (logic *DataLogic) Update(data req.DataUpdateReq) error {
	var dataData model.Data

	if err := utils.StructCopy(&dataData, &data); err != nil {
		return errors.DataLegalError()
	}

	if err := logic.Repository.Table(&model.Data{}).Where("id = ?", data.Id).Update(&dataData); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

func (logic *DataLogic) Set(data req.SetDataReq) error {
	if ok := utils.ContainsKeyInSlice(data.Value, []string{"status", "sort"}); ok {
		return errors.ContainsKeyError()
	}
	if err := logic.Repository.Table(&model.Data{}).Where("id = ?", data.Id).Update(&data.Value); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

// 删除广告
func (logic *DataLogic) Deleted(id int) error {
	if err := logic.Repository.Table(&model.Data{}).Where("id = ?", id).Deleted(); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

type DataLogic struct {
	Repository repository.IRepository `bean:"Repository"`
}
