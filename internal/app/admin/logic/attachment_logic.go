package logic

import (
	"errors"
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
)

// logic *AttachmentLogic IAttachmentLogic
type IAttachmentLogic interface {
	// 素材列表
	GetList(data req.AttachmentListReq) (interface{}, error)
	// 删除素材
	Deleted(id int) error
}

// 素材列表
func (logic *AttachmentLogic) GetList(data req.AttachmentListReq) (interface{}, error) {
	var list []res.AttachmentListRes

	if err := logic.Repository.Table(&model.File{}).Paging(data.Page, data.PageSize).OrderBy("id desc").GetList(&list); err != nil {
		return nil, errors.New("数据请求失败,错误代码：0x11FFF01")
	}
	return list, nil
}

// 删除素材
func (logic *AttachmentLogic) Deleted(id int) error {
	return logic.Repository.Table(&model.File{}).Where("id = ?", id).Deleted()
}

type AttachmentLogic struct {
	Repository repository.IRepository `bean:"Repository"`
}
