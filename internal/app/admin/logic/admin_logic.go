package logic

import (
	"errors"
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
	"whale/pkg/utils"
)

// generate logic *AdminLogic IAdminLogic
type IAdminLogic interface {
	// 账户列表
	GetList(data req.AccountListReq) (interface{}, error)
	// 添加账户
	GetInfo(id int) (interface{}, error)
	// 添加账户信息
	Create(data req.AccountCreateData) error
	// 更新账户信息
	Update(data req.AccountUpdateData) error
	// 删除账户信息
	Deleted(id int) error
	// 更新指定字段
	Set(dta req.AccountSetData) error
}

// 账户列表
func (logic *AdminLogic) GetList(data req.AccountListReq) (interface{}, error) {
	var accountList []res.AccountRecodRes
	dataBase := logic.Repository.Table(&model.Account{}).LikeSearch([]string{"account", "mobile", "nick_name"}, data.Keyword).Bentween("create_at", data.StartTime, data.EndTime).Eq("status", data.Status)
	if err := dataBase.Paging(data.Page, data.PageSize).OrderBy("id desc").GetList(&accountList); err != nil {
		return nil, errors.New("数据读取失败")
	}
	return accountList, nil
}

// 添加账户
func (logic *AdminLogic) GetInfo(id int) (interface{}, error) {
	var accountInfo res.AccountRecodRes
	if _, err := logic.Repository.Table(&model.Account{}).Where("id = ?", id).Get(&accountInfo); err != nil {
		return nil, errors.New("数据读取失败")
	}
	return accountInfo, nil
}

// 添加账户信息
func (logic *AdminLogic) Create(data req.AccountCreateData) error {
	var accountData model.Account
	dataBase := logic.Repository.Table(&accountData)

	if logic.Repository.Table(&accountData).Where("`account` = ?", data.Account).Exits() {
		return errors.New("登录账号不允许重复")
	}
	if logic.Repository.Table(&accountData).Where("`mobile` = ?", data.Mobile).Exits() {
		return errors.New("手机号码不允许重复")
	}
	// copy
	if err := utils.StructCopy(&data, &accountData); err != nil {
		return err
	}
	accountData.Salt = utils.GenerateRandomString(5)
	accountData.Status = 1
	accountData.Password = utils.PasswordMd5ToString("123456", accountData.Salt)
	return dataBase.Insert(&accountData)
}

// 更新账户信息
func (logic *AdminLogic) Update(data req.AccountUpdateData) error {
	var accountData model.Account
	dataBase := logic.Repository.Table(&accountData)

	if logic.Repository.Table(&accountData).Where("`account` = ? and id != ?", data.Account, data.Id).Exits() {
		return errors.New("登录账号不允许重复")
	}
	if logic.Repository.Table(&accountData).Where("`mobile` = ?  and id != ?", data.Mobile, data.Id).Exits() {
		return errors.New("手机号码不允许重复")
	}
	// copy
	if err := utils.StructCopy(&data, &accountData); err != nil {
		return err
	}

	if accountData.Password == "" {
		dataBase.Omit("password")
	} else {
		accountData.Salt = utils.GenerateRandomString(5)
		accountData.Password = utils.PasswordMd5ToString(data.Password, accountData.Salt)
	}
	return dataBase.Where("id = ?", data.Id).Update(&accountData)
}

// 删除账户信息
func (logic *AdminLogic) Deleted(id int) error {
	return logic.Repository.Table(&model.Account{}).Where("id = ?", id).Deleted()
}

// 更新指定字段
func (logic *AdminLogic) Set(data req.AccountSetData) error {
	if utils.ContainsKeyInSlice(data.Value, []string{"status", "verfiy_num"}) {
		return errors.New("请求失败,错误代码：0xFF101F1")
	}
	return logic.Repository.Table(&model.Account{}).Where("id = ?", data.Id).Update(data.Value)
}

type AdminLogic struct {
	Repository repository.IRepository `bean:"Repository"`
}
