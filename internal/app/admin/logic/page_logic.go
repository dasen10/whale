package logic

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/errors"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
	"whale/pkg/utils"
)

// logic *PageLogic IPageLogic
type IPageLogic interface {
	// 内容详情
	GetInfo(key string) (interface{}, error)
	// 更新单页内容
	Update(data req.PageUpdateReq) error
}

// 内容详情
func (logic *PageLogic) GetInfo(key string) (interface{}, error) {
	var info res.PageInfoRes
	if _, err := logic.Repository.Where("key = ?", key).Get(&info); err != nil {
		return nil, errors.DBReadError()
	}
	return info, nil
}

// 更新单页内容
func (logic *PageLogic) Update(data req.PageUpdateReq) error {
	var pageData req.PageUpdateReq

	if err := utils.StructCopy(&pageData, &data); err != nil {
		return errors.StructCopyError()
	}

	if err := logic.Repository.Table(&model.PageBlock{}).Where("id = ?", data.Id).Update(&pageData); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

type PageLogic struct {
	Repository repository.IRepository `bean:"Repository"`
}
