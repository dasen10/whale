package logic

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/errors"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
	"whale/pkg/utils"
)

// logic *DataKeyLogic IDataKeyLogic
type IDataKeyLogic interface {
	// 广告列表
	GetList(data req.DataKeyListReq) (interface{}, error)
	// 广告详情
	GetInfo(id int) (interface{}, error)
	// 创建广告
	Create(data req.DataKeyCreateReq) error
	// 更新广告
	Update(data req.DataKeyUpdateReq) error
	// 删除广告
	Deleted(id int) error
}

// 广告列表
func (logic *DataKeyLogic) GetList(data req.DataKeyListReq) (interface{}, error) {
	var list []res.DataKeyListRes
	if err := utils.StructCopy(&list, &data); err != nil {
		return nil, errors.DataLegalError()
	}

	if err := logic.Repository.Table(&model.DataKey{}).OrderBy("sort desc").GetList(&list); err != nil {
		return nil, errors.DBReadError()
	}
	return list, nil
}

// 广告详情
func (logic *DataKeyLogic) GetInfo(id int) (interface{}, error) {
	var info res.DataKeyInfoRes

	if _, err := logic.Repository.Table(&model.DataKey{}).Where("id = ?", id).Get(&info); err != nil {
		return nil, errors.DBReadError()
	}
	return info, nil
}

// 创建广告
func (logic *DataKeyLogic) Create(data req.DataKeyCreateReq) error {
	var dataKeyData model.DataKey

	if err := utils.StructCopy(&dataKeyData, &data); err != nil {
		return errors.DataLegalError()
	}

	if err := logic.Repository.Table(&model.DataKey{}).Insert(&dataKeyData); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

// 更新广告
func (logic *DataKeyLogic) Update(data req.DataKeyUpdateReq) error {
	var dataKeyData model.DataKey

	if err := utils.StructCopy(&dataKeyData, &data); err != nil {
		return errors.DataLegalError()
	}

	if err := logic.Repository.Table(&model.DataKey{}).Where("id = ?", data.Id).Update(&dataKeyData); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

// 删除广告
func (logic *DataKeyLogic) Deleted(id int) error {
	return logic.Repository.Table(&model.DataKey{}).Where("id = ?", id).Deleted()
}

type DataKeyLogic struct {
	Repository repository.IRepository `bean:"Repository"`
}
