package logic

import (
	"whale/internal/app/admin/consts/req"
	"whale/internal/app/admin/consts/res"
	"whale/internal/app/admin/errors"
	"whale/internal/app/admin/model"
	"whale/internal/repository"
	"whale/pkg/utils"
)

// generate logic *TaskLogic ITaskLogic
type ITaskLogic interface {
	// 任务列表
	GetList(data req.TaskListReq) (interface{}, error)
	// 任务详情
	GetInfo(id int) (interface{}, error)
	// 创建任务
	Create(data req.TaskCreateReq) error
	// 更新任务
	Update(data req.TaskUpdateReq) error
	// 删除任务
	Deleted(id int) error
	// 任务状态
	Set(data req.SetDataReq) error
	// 任务执行记录
	Oplogs(id int) (interface{}, error)
}

// 任务状态
func (logic *TaskLogic) Set(data req.SetDataReq) error {

	if ok := utils.ContainsKeyInSlice(data.Value, []string{"status", "sort"}); ok {
		return errors.ContainsKeyError()
	}

	if err := logic.Repository.Table(&model.Queue{}).Where("id = ?", data.Id).Update(data.Value); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

// 任务列表
func (logic *TaskLogic) GetList(data req.TaskListReq) (interface{}, error) {
	var list model.Queue

	dataBases := logic.Repository.Table(&model.Queue{}).LikeSearch([]string{"code", "title"}, data.Keyword).Eq("status", data.Status)

	if err := dataBases.Paging(data.Page, data.PageSize).OrderBy("id desc").GetList(&list); err != nil {
		return nil, errors.DBReadError()
	}
	return list, nil
}

// 任务详情
func (logic *TaskLogic) GetInfo(id int) (interface{}, error) {
	var info res.TaskInfoRes

	if _, err := logic.Repository.Table(&model.Queue{}).Where("id = ?", id).Get(&info); err != nil {
		return nil, errors.DBReadError()
	}
	return info, nil
}

// 创建任务
func (logic *TaskLogic) Create(data req.TaskCreateReq) error {
	var taskData model.Queue

	if err := utils.StructCopy(&taskData, &data); err != nil {
		return errors.StructCopyError()
	}

	if err := logic.Repository.Table(&model.Queue{}).Insert(&taskData); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

// 更新任务
func (logic *TaskLogic) Update(data req.TaskUpdateReq) error {
	var taskData model.Queue

	if err := utils.StructCopy(&taskData, &data); err != nil {
		return errors.StructCopyError()
	}

	if err := logic.Repository.Table(&model.Queue{}).Where("id = ?", data.Id).Update(&taskData); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

// 删除任务
func (logic *TaskLogic) Deleted(id int) error {
	if err := logic.Repository.Table(&model.Queue{}).Where("id = ?", id).Deleted(); err != nil {
		return errors.DBStoreError()
	}
	return nil
}

// 任务执行记录
func (logic *TaskLogic) Oplogs(id int) (interface{}, error) {
	return nil, nil
}

type TaskLogic struct {
	Repository repository.IRepository `bean:"Repository"`
}
