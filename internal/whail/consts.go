package w

// 公共请求参数
type PagingReq struct {
	Page     int `validate:"required|integer" query:"page" json:"page,omitempty"`
	PageSize int `validate:"required|integer" query:"page_size" json:"page_size,omitempty"`
}

// 公共请求参数
type SetDataReq struct {
	Id    int                    `validate:"required" json:"id,omitempty"`
	Value map[string]interface{} `validate:"required" json:"value,omitempty"`
}

// 公共请求参数
type ByIdReq struct {
	Id int `validate:"required" json:"id,omitempty"`
}

// 公共请求参数
type SearchKeywordReq struct {
	Keyword   string `json:"keyword,omitempty" validate:"-"`
	StartTime string `json:"start_time,omitempty" validate:"-"`
	EndTime   string `validate:"-" json:"end_time,omitempty"`
	Status    int    `validate:"-" json:"status,omitempty"`
}
