package main

import (
	"whale/internal"
)

func main() {
	// 加载框架
	/* 后期可以通过该接口进行配置注入，打达到快速部署的目的 */
	internal.SingletonModel()
}
