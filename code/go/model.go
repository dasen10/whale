package code

import "whale/internal/models"

// 管理员表
type Admin struct {
	models.BaseModel `xorm:"extends"`
	Mobile           string `xorm:"varchar(120) notnull unique comment('管理员手机号码')" json:"mobile"`
	Account          string `xorm:"varchar(120) notnull unique comment('管理员账号')" json:"account"`
	Password         string `xorm:"varchar(200) notnull comment('登录密码')" json:"password"`
	NickName         string `xorm:"varchar(120) null comment('管理员昵称')" json:"nick_name"`
	Headimg          string `xorm:"varchar(200) null comment('管理员头像')" json:"headimg"`
	Authorize        string `xorm:"varchar(120) notnull comment('管理员权限标识')" json:"authorize"`
	LoginIp          string `xorm:"varchar(120) null comment('登录ip')" json:"login_ip"`
	LoginNum         int    `xorm:" default(0) comment('登录次数')" json:"login_num"`
	// 超过5次禁止登录
	VerfiyNum int             `xorm:" default(0) comment('登录失误次数')" json:"verfiy_num"`
	LoginAt   models.XormTime `xorm:"null comment('登录时间')" json:"login_at,omitempty"`
	Describe  string          `xorm:"varchar(500) null comment('备注信息')" json:"describe"`
	Status    int             `xorm:"tinyint(3) default(1) comment('账号状态')" json:"status"`
	Sort      int             `xorm:"tinyint(3) default(255) comment('排序规则')" json:"sort"`
}
