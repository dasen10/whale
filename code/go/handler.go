package code

import (
	"whale/internal/app/admin/logic"

	"github.com/gofiber/fiber/v2"
)

type IWork interface {
	// 测试请求
	Test(ctx *fiber.Ctx) error
}

func (work *WorkHandler) Test(ctx *fiber.Ctx) error {
	work.AdminLogic.GetInfo(1)
	return ctx.JSON(map[string]interface{}{"code": 200, "message": "success", "data": nil})
}

type WorkHandler struct {
	AdminLogic logic.IAdminLogic `bean:"AdminLogic"`
}
