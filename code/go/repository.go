package code

import (
	"whale/internal/app/admin/model"
	"whale/internal/load"
)

// 权限管理
type AdminAuthRepository struct {
	DB load.DBConnect `bean:"DBConnect"`
}

// 添加权限节点
func (auth *AdminAuthRepository) AddAdminAuth() error {
	panic("not implemented") // TODO: Implement
}

// 编辑权限节点
func (auth *AdminAuthRepository) EditAdminAuth() error {
	panic("not implemented") // TODO: Implement
}

// 删除权限节点
func (auth *AdminAuthRepository) DeletedAdminAuth(id int) error {
	panic("not implemented") // TODO: Implement
}

// 查询权限详情
func (auth *AdminAuthRepository) QueryAdminAuthInfo(id int) (*model.AdminAuth, error) {
	panic("not implemented") // TODO: Implement
}

// 查询权限列表
func (auth *AdminAuthRepository) QueryAdminAuthList() ([]*model.AdminAuth, error) {
	panic("not implemented") // TODO: Implement
}

// 权限管理
type IAdminAuthRepository interface {
	// 添加权限节点
	AddAdminAuth() error
	// 编辑权限节点
	EditAdminAuth() error
	// 删除权限节点
	DeletedAdminAuth(id int) error
	// 查询权限详情
	QueryAdminAuthInfo(id int) (*model.AdminAuth, error)
	// 查询权限列表
	QueryAdminAuthList() ([]*model.AdminAuth, error)
}
