package code

import (
	"whale/internal/app/admin/model"
	"whale/internal/app/admin/repository"
)

type IWorkLogic interface {
	Test() error
}

func (logic *WorkLogic) Test() error {
	var data = &model.Admin{}
	return logic.AdminRepo.AddAdmin(data)
}

type WorkLogic struct {
	AdminRepo repository.IAdminRepository `bean:"AdminRepository"`
}
