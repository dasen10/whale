package code

import (
	"whale/internal/app/admin/handler"
	"whale/internal/app/admin/logic"
	"whale/internal/app/admin/repository"
	"whale/pkg/bean"

	"github.com/gofiber/fiber/v2"
)

type IAdminRouter interface {
	AdminRouterInit(app *fiber.App)
}

type AdminRouter struct {
	WorkHandler handler.IWork `bean:"WorkHandler"`
}

func (admin *AdminRouter) AdminRouterInit(app *fiber.App) {

	adminApi := app.Group("/admin/v1/api")
	{
		adminApi.Get("/work", admin.WorkHandler.Test)
		adminApi.Get("/demo", admin.WorkHandler.Test)
	}
}

func NewAdminRouter(beanContainer *bean.BeanContainer) *AdminRouter {
	beanContainer.SetSingleton("AdminRepository", &repository.AdminRepository{})
	beanContainer.SetSingleton("WorkLogic", &logic.WorkLogic{})
	beanContainer.SetSingleton("WorkHandler", &handler.WorkHandler{})
	return &AdminRouter{}
}
