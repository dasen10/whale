package validate

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"github.com/gookit/validate"
)

// 验证结构体
func ValidateJsonStruct(ctx *fiber.Ctx, data interface{}) error {
	if err := ctx.BodyParser(data); err != nil {
		return err
	}

	v := validate.Struct(data)
	if v.Validate() {
		return nil
	}
	return errors.New(v.Errors.One())
}

// 解析结构体
func ValidateQueryStruct(ctx *fiber.Ctx, data interface{}) error {
	if err := ctx.QueryParser(data); err != nil {
		return err
	}
	v := validate.Struct(data)
	if v.Validate() {
		return nil
	}
	return errors.New(v.Errors.One())
}
