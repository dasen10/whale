package response

import "github.com/gofiber/fiber/v2"

// 成功
func Success(ctx *fiber.Ctx, data interface{}) error {
	return ctx.JSON(map[string]interface{}{
		"code":    10000,
		"message": "success",
		"data":    data,
	})
}

// 失败
func Error(ctx *fiber.Ctx, code int, message string) error {
	return ctx.JSON(map[string]interface{}{
		"code":    code,
		"message": message,
		"data":    nil,
	})
}
