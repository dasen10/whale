package file

import (
	"mime/multipart"
	"net/http"
	"os"
)

// 判断路径是否存在
func MkdirExist(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		// 不存在 创建
		return os.MkdirAll(path, os.ModePerm)
	}
	return nil
}

// 读取文件类型
func GetFileType(file *multipart.FileHeader) (string, error) {
	f, err := file.Open()

	if err != nil {
		return "", err
	}
	defer f.Close()

	buffer := make([]byte, 512)

	if _, err = f.Read(buffer); err != nil {
		return "", err
	}
	fileType := http.DetectContentType(buffer)
	return fileType, nil
}
